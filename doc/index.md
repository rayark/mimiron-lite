# Mimiron Lite

Configurable Unity command line build interface

#### Usage Sample

```csharp
using Rayark.MimironLite.Build;
using Rayark.MimironLite.UI;

public class AndroidBuildExample
{
    // Command Line entry point
    public static void Build()
    {
        Build(Environment.GetCommandLineArgs());
    }

    public static void Build(string[] arguments)
    {
        var builder = CreateBuilder();

        var args = new CommandLineArguments(arguments);
        builder.Build(args);
    }

    [BuilderCreation]
    private static Builder CreateBuilder()
    {
        var builder = Builder.Create(BuildTarget.Android, "Android Builder Example");

        return builder.AddSetBuildTargetPathStep()
                      .AddSetDevelopmentStep()
                      .AddSetBundleIdentifierStep()
                      .AddSetAndroidKeyStoreStep()
                      .AddAndroidVersionNameStep()
                      .AddAndroidVersionCodeStep();
    }
}
```

Obtain command line arguments in string[] with [Environment.GetCommandLineArgs()](https://docs.microsoft.com/en-us/dotnet/api/system.environment.getcommandlineargs) and pass it to [MimironLite.CommandLineArguments](xref:Rayark.MimironLite.CommandLineArguments) constructor.

Call [Builder.Create](xref:Rayark.MimironLite.Build.Builder.Create(UnityEditor.BuildTarget,System.String)) to create a builder and add build steps to it.

Once build steps are configured, call [Builder.Build()](xref:Rayark.MimironLite.Build.Builder.Build(Rayark.MimironLite.CommandLineArguments)) with CommandLineArguments.

Mimiron Lite provides automatic building UI generation. If you would like your builder to be invokable from Editor, declare a static function that returns configured builder object and tag it with BuilderCreationAttribute. The usage sample demonstrates a pattern for the builder to be invokable from both command-line and Unity editor.

#### Build Step Sample
```csharp
public class SetDevelopmentBuildStep : IBuildStep
{
    private class Parameter : IBuildStepParameter
    {
        [CommandlineFlag("-development")]
        public bool IsDevelopmentBuild;
    }

    public void Execute
    (
        CommandLineArguments       commandLineArguments,
        IPlayerSettingsManipulator playerSettings,
        BuildTargetPathTracker     buildTargetPathTracker,
        BuildOptionsTracker        buildOptionsTracker
    )
    {
        var parameter = new Parameter();
        ParameterUtilities.ParseParameters(commandLineArguments, parameter);

        if (parameter.IsDevelopmentBuild)
        {
            buildOptionsTracker.Add(BuildOptions.Development);
        }
        else
        {
            buildOptionsTracker.Remove(BuildOptions.Development);
        }
    }

    public BuildTargetCollection GetApplicableBuildTargets()
    {
        return BuildTargetCollection.Any;
    }

    public BuildStepMetadata GetMetadata()
    {
        const string description = "Set is development build";
        var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

        return new BuildStepMetadata(description, parameterList);
    }
}
```

Build step must implements [IBuildStep](xref:Rayark.MimironLite.BuildStep.IBuildStep) interface, and do the argument parsing as well as execution in [IBuildStep.Execute](xref:Rayark.MimironLite.BuildStep.IBuildStep.Execute(Rayark.MimironLite.CommandLineArguments,Rayark.MimironLite.PlayerSettings.IPlayerSettingsManipulator,Rayark.MimironLite.Build.BuildTargetPathTracker,Rayark.MimironLite.Build.BuildOptionsTracker)). If the build step doesn't require special argument parsing logic, you can declare an inner class inherits [IBuildStepParameter](xref:Rayark.MimironLite.IBuildStepParameter) and get the argument values with [ParameterUtilities.ParseParameters](xref:Rayark.MimironLite.ParameterUtilities.ParseParameters(Rayark.MimironLite.CommandLineArguments,Rayark.MimironLite.IBuildStepParameter)). ParameterUtilities.ParseParameters will retrieve values with reflection for any field tagged with [CommandlineFlagAttribute](xref:Rayark.MimironLite.CommandlineFlagAttribute).

ParameterUtilities and CommandlineFlagAttribute support types of:

* int
* float
* string (Must non-empty)
* bool (only "true" and "false", no other capitalization)

[IBuildStep.GetLimitedBuildTargetPlatform](xref:Rayark.MimironLite.BuildStep.IBuildStep.GetApplicableBuildTargets) should return platforms this build step supports, if there is no limit on platforms then it should return BuildTargetCollection.Any.

[IBuildStep.GetMetadata](xref:Rayark.MimironLite.BuildStep.IBuildStep.GetMetadata) should return descriptions of build step for generating Unity editor UI and command-line hints.

#### Generated Unity Editor UI

Menu path:

"Rayark\Mimiron Lite\Editor Builder"

![builderUI](./images/builderUI.png)

The behavior of invoking builder here will be identical to invoking form command-line. The UI will cache entered value for fast rebuild.

#### Built-in Build Steps

* Common
  * [SetBuildTargetPathBuildStep](xref:Rayark.MimironLite.BuildStep.SetBuildTargetPathBuildStep) [-targetPath]  
    Set build target path
  * [SetBundleIdentifierBuildStep](xref:Rayark.MimironLite.BuildStep.SetBundleIdentifierBuildStep) [-bundleIdentifier]   
    Set identifier
  * [SetDefineSymbolBuildStep](xref:Rayark.MimironLite.BuildStep.SetDefineSymbolBuildStep) [custom flag]  
    Add define symbol when argument is true, remove on false
  * [SetDevelopmentBuildStep](xref:Rayark.MimironLite.BuildStep.SetDevelopmentBuildStep) [-development]  
    Set is development build or not
  * [SetGpuSkinningBuildStep](xref:Rayark.MimironLite.BuildStep.SetGpuSkinningBuildStep) [-GPUSkinning]  
    Set is GPU skinning is enabled or not
* Android
  * [SetAndroidVersionNameBuildStep](xref:Rayark.MimironLite.BuildStep.Android.SetAndroidVersionNameBuildStep) [-androidVersionName]  
    Set Android Version Name (String, display on user devices)
  * [SetAndroidVersionCodeBuildStep](xref:Rayark.MimironLite.BuildStep.Android.SetAndroidVersionCodeBuildStep) [-androidVersionCode]  
    Set Android Version Code (Integer, only visible to developers)
  * [SetAndroidApkExpansionFilesBuildStep](xref:Rayark.MimironLite.BuildStep.Android.SetAndroidApkExpansionFilesBuildStep) [-generateOBB]  
    Set is split OBB build or not
  * [SetAndroidKeyStoreBuildStep](xref:Rayark.MimironLite.BuildStep.Android.SetAndroidKeyStoreBuildStep) [-keystoreName -keystorePass -keyaliasName -keyaliasPass]  
    Set unlock keystore username and password
  * [SetAndroidSdkRootPathBuildStep](xref:Rayark.MimironLite.BuildStep.Android.SetAndroidSdkRootPathBuildStep) [-androidSDKRoot]  
    Set Android SDK path
  * [SetAndroidNdkRootPathBuildStep](xref:Rayark.MimironLite.BuildStep.Android.SetAndroidNdkRootPathBuildStep) [-androidNDKRoot]  
    Set Android NDK path
  * [SetIsExportingGradleProjectBuildStep](xref:Rayark.MimironLite.BuildStep.Android.SetIsExportingGradleProjectBuildStep) [-exportGradle]  
    Set is exporting Gradle project or not
* iOS
  * [SetiOSVersionStringBuildStep](xref:Rayark.MimironLite.BuildStep.iOS.SetiOSVersionStringBuildStep) [-iOSVersionString]  
    Set iOS Version String (x.y.z format, has to match current iTunes Connect settings, displayed as Version in Xcode, plist key: CFBundleShortVersionString)
  * [SetiOSBuildStringBuildStep](xref:Rayark.MimironLite.BuildStep.iOS.SetiOSBuildStringBuildStep) [-iOSBuildString]  
    Set iOS Build String (x.y.z format, displayed as Build in Xcode, plist key: CFBundleVersion)



