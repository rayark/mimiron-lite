# Mimiron Lite

Configurable Unity command line build interface

[![pipeline status](https://gitlab.com/rayark/mimiron-lite/badges/master/pipeline.svg)](https://gitlab.com/rayark/mimiron-lite/commits/master)

- [Official Repository (Gitlab)](https://gitlab.com/rayark/mimiron-lite)
- Documentation

#### Setup

Extract source code to your Unity Project

#### Usage Sample

```csharp
using Rayark.MimironLite.Build;
using Rayark.MimironLite.UI;

public class AndroidBuildExample
{
    // Command Line entry point
    public static void Build()
    {
        Build(Environment.GetCommandLineArgs());
    }

    public static void Build(string[] arguments)
    {
        var builder = CreateBuilder();

        var args = new CommandLineArguments(arguments);
        var report = builder.Build(args);
        EditorApplication.Exit(report.summary.result == BuildResult.Succeeded ? 0 : 1);
    }

    [BuilderCreation]
    private static Builder CreateBuilder()
    {
        var builder = Builder.Create(BuildTarget.Android, "Android Builder Example");

        return builder.AddSetBuildTargetPathStep()
                      .AddSetDevelopmentStep()
                      .AddSetBundleIdentifierStep()
                      .AddSetAndroidKeyStoreStep()
                      .AddAndroidVersionNameStep()
                      .AddAndroidVersionCodeStep();
    }
}
```

Obtain command line arguments in `string[]` with [Environment.GetCommandLineArgs()](https://docs.microsoft.com/en-us/dotnet/api/system.environment.getcommandlineargs) and pass it to `MimironLite.CommandLineArguments` constructor.

Call `Builder.Create` to create a `Builder` and add build steps to it.

Once build steps are configured, call `Builder.Build()` with `CommandLineArguments`. You might need to examine build report returned by `Builder.Build()` and call `EditorApplication.Exit` accordingly to control return value of Unity executable so that your multi-step CI pipeline would break on error. Unity explained why they don't set the value for your [here](https://support.unity.com/hc/en-us/articles/211195263-Why-doesn-t-a-failed-BuildPipeline-BuildPlayer-return-an-error-code-in-the-command-line-) Mimiron cannot call `EditorApplication.Exit` for you, calling `EditorApplication.Exit` will terminate Unity process immediately and skip any code after `Builder.Build()`.

Mimiron Lite provides automatic building UI generation. If you would like your builder to be invokable from Unity editor, declare a static function that returns configured builder object and tag it with `BuilderCreationAttribute`. The usage sample demonstrates a pattern for the builder to be invokable from both command-line and Unity editor.

#### Build Step Sample
```csharp
public class SetDevelopmentBuildStep : IBuildStep
{
    private class Parameter : IBuildStepParameter
    {
        [CommandlineFlag("-development")]
        public bool IsDevelopmentBuild;
    }

    public void Execute
    (
        CommandLineArguments       commandLineArguments,
        IPlayerSettingsManipulator playerSettings,
        BuildTargetPathTracker     buildTargetPathTracker,
        BuildOptionsTracker        buildOptionsTracker
    )
    {
        var parameter = new Parameter();
        ParameterUtilities.ParseParameters(commandLineArguments, parameter);

        if (parameter.IsDevelopmentBuild)
        {
            buildOptionsTracker.Add(BuildOptions.Development);
        }
        else
        {
            buildOptionsTracker.Remove(BuildOptions.Development);
        }
    }

    public BuildTargetCollection GetApplicableBuildTargets()
    {
        return BuildTargetCollection.Any;
    }

    public BuildStepMetadata GetMetadata()
    {
        const string description = "Set is development build";
        var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

        return new BuildStepMetadata(description, parameterList);
    }
}
```

Build step must implements `IBuildStep` interface, and do the argument parsing as well as execution in `IBuildStep.Execute`. If the build step doesn't require special argument parsing logic, you can declare an inner class inherits `IBuildStepParameter` and get the argument values with `ParameterUtilities.ParseParameters`. `ParameterUtilities.ParseParameters` will retrieve values with reflection for any field tagged with `CommandlineFlagAttribute`.

`ParameterUtilities` and `CommandlineFlagAttribute` support types of:

* `int`
* `float`
* `string` (Must non-empty)
* `bool` (only "true" and "false", no other capitalization)

`IBuildStep.GetLimitedBuildTargetPlatform` should return platforms this build step supports, if there is no limit on platforms then it should return `BuildTargetCollection.Any`.

`IBuildStep.GetMetadata` should return descriptions of build step for generating Unity editor UI and command-line hints.

#### Generated Unity Editor UI

Menu path:

"Rayark\Mimiron Lite\Editor Builder"

![builderUI](./Documents/builderUI.png)

The behavior of invoking builder here will be identical to invoking form command-line. The UI will cache entered value for fast rebuild.

#### Built-in Build Steps

- Common
  - `SetBuildTargetPathBuildStep` [-targetPath]  
    Set build target path
  - `SetBundleIdentifierBuildStep` [-bundleIdentifier]   
    Set identifier
  - `SetDefineSymbolBuildStep` [custom flag]  
    Add define symbol when argument is true, remove on false
  - `SetDevelopmentBuildStep` [-development]  
    Set is development build or not
  - `SetGpuSkinningBuildStep` [-GPUSkinning]  
    Set is GPU skinning is enabled or not
- Android
  - `SetAndroidVersionNameBuildStep` [-androidVersionName]  
    Set Android Version Name (String, display on user devices)
  - `SetAndroidVersionCodeBuildStep` [-androidVersionCode]  
    Set Android Version Code (Integer, only visible to developers)
  - `SetAndroidApkExpansionFilesBuildStep` [-generateOBB]  
    Set is split OBB build or not
  - `SetAndroidKeyStoreBuildStep` [-keystoreName -keystorePass -keyaliasName -keyaliasPass]  
    Set unlock keystore username and password
  - `SetAndroidSdkRootPathBuildStep` [-androidSDKRoot]  
    Set Android SDK path
  - `SetAndroidNdkRootPathBuildStep` [-androidNDKRoot]  
    Set Android NDK path
  - `SetIsExportingGradleProjectBuildStep` [-exportGradle]  
    Set is exporting Gradle project or not
  - `SetIsBuildingApkPerCpuArchitectureBuildStep` [-multipleAPKsPerABI]
    Set is building multiple APKs per ABI or not
- iOS
  - `SetiOSVersionStringBuildStep` [-iOSVersionString]  
    Set iOS Version String (x.y.z format, has to match current iTunes Connect settings, displayed as Version in Xcode, plist key: `CFBundleShortVersionString`)
  - `SetiOSBuildStringBuildStep` [-iOSBuildString]  
    Set iOS Build String (x.y.z format, displayed as Build in Xcode, plist key: `CFBundleVersion`)