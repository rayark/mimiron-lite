#if UNITY_5 || UNITY_5_3_OR_NEWER
#define UNITY_5_OR_NEWER
#endif

#if UNITY_5_1_1 || UNITY_5_1_2 || UNITY_5_1_3 || UNITY_5_1_4 || UNITY_5_1_5 || UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_1_1_OR_NEWER
#endif

#if UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_2_OR_NEWER
#endif


#if UNITY_5_OR_NEWER
#define ANDROID_BUILD_SUBTARGET_RENAMED_TO_MOBILE_TEXTURE_SUBTARGET
#define EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE
#endif

#if UNITY_5_1_1_OR_NEWER
#define GPU_SKINNING_SETTER_AVAILABLE
#endif

#if UNITY_5_2_OR_NEWER
#define IOS_BUILD_NUMBER_AVAILABLE
#endif

#if UNITY_5_5_OR_NEWER
#define ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
#endif

#if UNITY_5_6_OR_NEWER
#define BUNDLE_IDENTIFIER_RENAMED_TO_APPLICATION_IDENTIFIER
#endif

#if UNITY_2018_2_OR_NEWER
#define BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
#endif

#if UNITY_2018_3_OR_NEWER
#define NDK_R16B
#endif

#if UNITY_2019_3_OR_NEWER
#define NDK_R19
#endif


using System;

using UnityEditor;

namespace Rayark.MimironLite.PlayerSettings
{
    using PlayerSettings = UnityEditor.PlayerSettings;

    /// <summary>
    /// Implementation of PlayerSettings modification
    /// </summary>
    internal class PlayerSettingsManipulator : IPlayerSettingsManipulator
    {
        public BuildTargetGroup TargetGroup { get; private set; }

        public PlayerSettingsManipulator(BuildTarget target)
        {
            try
            {
                TargetGroup = (BuildTargetGroup) Enum.Parse(typeof(BuildTargetGroup), target.ToString());
            }
            catch (ArgumentException e)
            {
                if (target.ToString().ToLower().Contains("standalone"))
                {
                    TargetGroup = BuildTargetGroup.Standalone;
                }
                else
                {
                    throw e;
                }
            }
        }


        string IPlayerSettingsManipulator.BundleIdentifier
        {
#if BUNDLE_IDENTIFIER_RENAMED_TO_APPLICATION_IDENTIFIER
            get
            {
                BuildTarget activeBuildTarget = EditorUserBuildSettings.activeBuildTarget;
                BuildTargetGroup activeBuildTargetGroup = BuildPipeline.GetBuildTargetGroup(activeBuildTarget);

                return PlayerSettings.GetApplicationIdentifier(activeBuildTargetGroup);
            }
            set
            {
                BuildTarget activeBuildTarget = EditorUserBuildSettings.activeBuildTarget;
                BuildTargetGroup activeBuildTargetGroup = BuildPipeline.GetBuildTargetGroup(activeBuildTarget);

                PlayerSettings.SetApplicationIdentifier(activeBuildTargetGroup, value);
            }
#else
            get { return PlayerSettings.bundleIdentifier; }
            set { PlayerSettings.bundleIdentifier = value; }
#endif
        }

        string IPlayerSettingsManipulator.BundleVersion
        {
            get { return PlayerSettings.bundleVersion; }
            set { PlayerSettings.bundleVersion = value; }
        }

#if GPU_SKINNING_SETTER_AVAILABLE
        bool IPlayerSettingsManipulator.IsGpuSkinning
        {
            get { return PlayerSettings.gpuSkinning; }
            set { PlayerSettings.gpuSkinning = value; }
        }
#endif


        string IPlayerSettingsManipulator.GetDefineSymbols()
        {
            return PlayerSettings.GetScriptingDefineSymbolsForGroup(TargetGroup);
        }

        void IPlayerSettingsManipulator.SetDefineSymbols(string symbol)
        {
            PlayerSettings.SetScriptingDefineSymbolsForGroup(TargetGroup, symbol);
        }


        int IPlayerSettingsManipulator.AndroidVersionCode
        {
            get { return PlayerSettings.Android.bundleVersionCode; }
            set { PlayerSettings.Android.bundleVersionCode = value; }
        }

#if ANDROID_BUILD_SUBTARGET_RENAMED_TO_MOBILE_TEXTURE_SUBTARGET
        MobileTextureSubtarget IPlayerSettingsManipulator.AndroidTextureSubtarget
#else
        AndroidBuildSubtarget IPlayerSettingsManipulator.AndroidTextureSubtarget
#endif
        {
            get { return EditorUserBuildSettings.androidBuildSubtarget; }
            set { EditorUserBuildSettings.androidBuildSubtarget = value; }
        }

        bool IPlayerSettingsManipulator.IsUsingApkExpansionFiles
        {
            get { return PlayerSettings.Android.useAPKExpansionFiles; }
            set { PlayerSettings.Android.useAPKExpansionFiles = value; }
        }


        string IPlayerSettingsManipulator.AndroidSdkRootPath
        {
            set { EditorPrefs.SetString("AndroidSdkRoot", value); }
        }

        // https://docs.unity3d.com/Manual/android-sdksetup.html
        // This list is wrong about Unity 2018.3, which is using r16b
        // https://unity3d.com/unity/whats-new/unity-2018.3.0
        string IPlayerSettingsManipulator.AndroidNdkRootPath
        {
            set
            {
#if NDK_R19
                EditorPrefs.SetString("AndroidNdkRootR19", value);
#elif NDK_R16B
                EditorPrefs.SetString("AndroidNdkRootR16b", value);
#else
                EditorPrefs.SetString("AndroidNdkRoot", value);
#endif
            }
        }

        bool IPlayerSettingsManipulator.UseCustomKeystore
        {
            set
            {
#if UNITY_2019_1_OR_NEWER
                PlayerSettings.Android.useCustomKeystore = value;
#endif
            }
        }

        string IPlayerSettingsManipulator.AndroidKeystoreName
        {
            set { PlayerSettings.Android.keystoreName = value; }
        }

        string IPlayerSettingsManipulator.AndroidKeystorePass
        {
            set { PlayerSettings.Android.keystorePass = value; }
        }

        string IPlayerSettingsManipulator.AndroidKeyaliasName
        {
            set { PlayerSettings.Android.keyaliasName = value; }
        }

        string IPlayerSettingsManipulator.AndroidKeyaliasPass
        {
            set { PlayerSettings.Android.keyaliasPass = value; }
        }


        #region Android
#if ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
        AndroidBuildSystem IPlayerSettingsManipulator.AndroidBuildSystem
        {
            get { return EditorUserBuildSettings.androidBuildSystem; }
            set { EditorUserBuildSettings.androidBuildSystem = value; }
        }
#endif

#if EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE
        bool IPlayerSettingsManipulator.IsExportingAsGoogleAndroidProject
        {
            get { return EditorUserBuildSettings.exportAsGoogleAndroidProject; }
            set { EditorUserBuildSettings.exportAsGoogleAndroidProject = value; }
        }
#endif

#if BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
        bool IPlayerSettingsManipulator.IsBuildingApkPerCpuArchitecture
        {
            get { return PlayerSettings.Android.buildApkPerCpuArchitecture; }
            set { PlayerSettings.Android.buildApkPerCpuArchitecture = value; }
        }
#endif
        #endregion


        #region iOS
#if IOS_BUILD_NUMBER_AVAILABLE
        string IPlayerSettingsManipulator.iOSBuildNumber
        {
            get { return PlayerSettings.iOS.buildNumber; }
            set { PlayerSettings.iOS.buildNumber = value; }
        }
#endif
        #endregion
    }
}