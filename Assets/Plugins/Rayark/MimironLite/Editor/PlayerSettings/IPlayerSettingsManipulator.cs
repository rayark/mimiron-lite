#if UNITY_5 || UNITY_5_3_OR_NEWER
#define UNITY_5_OR_NEWER
#endif

#if UNITY_5_1_1 || UNITY_5_1_2 || UNITY_5_1_3 || UNITY_5_1_4 || UNITY_5_1_5 || UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_1_1_OR_NEWER
#endif

#if UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_2_OR_NEWER
#endif


#if UNITY_5_OR_NEWER
#define ANDROID_BUILD_SUBTARGET_RENAMED_TO_MOBILE_TEXTURE_SUBTARGET
#define EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE
#endif

#if UNITY_5_1_1_OR_NEWER
#define GPU_SKINNING_SETTER_AVAILABLE
#endif

#if UNITY_5_2_OR_NEWER
#define IOS_BUILD_NUMBER_AVAILABLE
#endif

#if UNITY_5_5_OR_NEWER
#define ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
#endif

#if UNITY_5_6_OR_NEWER
#define BUNDLE_IDENTIFIER_RENAMED_TO_APPLICATION_IDENTIFIER
#endif

#if UNITY_2018_2_OR_NEWER
#define BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
#endif

using UnityEditor;


namespace Rayark.MimironLite.PlayerSettings
{
    /// <summary>
    /// Interface of Unity PlayerSettings modifications
    /// </summary>
    public interface IPlayerSettingsManipulator
    {
        string BundleIdentifier { get; set; }
        string BundleVersion    { get; set; }
#if GPU_SKINNING_SETTER_AVAILABLE
        bool IsGpuSkinning { get; set; }
#endif

        void   SetDefineSymbols(string symbol);
        string GetDefineSymbols();


        int  AndroidVersionCode { get; set; }
        bool IsUsingApkExpansionFiles { get; set; }

        string AndroidSdkRootPath  { set; }
        string AndroidNdkRootPath  { set; }

        bool UseCustomKeystore { set; }
        /// <value>Path to Android keystore file</value> 
        string AndroidKeystoreName { set; }
        string AndroidKeystorePass { set; }
        string AndroidKeyaliasName { set; }
        string AndroidKeyaliasPass { set; }

#if ANDROID_BUILD_SUBTARGET_RENAMED_TO_MOBILE_TEXTURE_SUBTARGET
        MobileTextureSubtarget AndroidTextureSubtarget { get; set; }
#else
        AndroidBuildSubtarget AndroidTextureSubtarget { get; set; }
#endif

#if ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
        AndroidBuildSystem AndroidBuildSystem { get; set; }
#endif
#if EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE
        bool IsExportingAsGoogleAndroidProject { get; set; }
#endif
#if BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
        bool IsBuildingApkPerCpuArchitecture { get; set; }
#endif

#if IOS_BUILD_NUMBER_AVAILABLE
        string iOSBuildNumber { get; set; }
#endif
    }
}