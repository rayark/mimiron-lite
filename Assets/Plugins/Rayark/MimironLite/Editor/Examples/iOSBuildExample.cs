using System;

using UnityEditor;
using UnityEditor.Build.Reporting;

namespace Rayark.MimironLite.Example
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.UI;

    public class iOSBuildExample
    {
        // Command Line entry point
        public static void Build()
        {
            Build(Environment.GetCommandLineArgs());
        }

        public static void Build(string[] arguments)
        {
            var builder = CreateBuilder();

            var args = new CommandLineArguments(arguments);
            var report = builder.Build(args);
            EditorApplication.Exit(report.summary.result == BuildResult.Succeeded ? 0 : 1);
        }

        [BuilderCreation]
        private static Builder CreateBuilder()
        {
            var builder = Builder.Create(BuildTarget.iOS, "iOS Builder Example");

            return builder.AddSetBuildTargetPathStep()
                          .AddSetDevelopmentStep()
                          .AddSetBundleIdentifierStep()
                          .AddSetiOSVersionStringStep()
                          .AddSetiOSBuildStringStep();
        }
    }
}