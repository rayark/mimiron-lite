#if UNITY_5_3_OR_NEWER
#define JSON_UTILITY_AVAILABLE
#endif

using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;


namespace Rayark.MimironLite.UI
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;

    using FlagToCachedArgumentValueDictionary = Dictionary<string, EditorBuilder.CachedArgumentValue>;

    /// <summary>
    /// Unity editor building UI
    /// </summary>
    public class EditorBuilder : EditorWindow
    {
        [Serializable]
        public class CachedArgumentValue
        {
            public ParameterType Type;

            public string CachedStringValue;
            public int    CachedIntValue;
            public float  CachedFloatValue;
            public bool   CachedBoolValue;

            public void CopyFrom(CachedArgumentValue sourceCachedValue)
            {
                if (Type != sourceCachedValue.Type)
                {
                    return;
                }

                switch (Type)
                {
                    case ParameterType.String:
                        CachedStringValue = sourceCachedValue.CachedStringValue;
                        break;
                    case ParameterType.Bool:
                        CachedBoolValue = sourceCachedValue.CachedBoolValue;
                        break;
                    case ParameterType.Int:
                        CachedIntValue = sourceCachedValue.CachedIntValue;
                        break;
                    case ParameterType.Float:
                        CachedFloatValue = sourceCachedValue.CachedFloatValue;
                        break;
                    default: break;
                }
            }
        }

        private class BuilderCachedArguments
        {
            private readonly FlagToCachedArgumentValueDictionary _flagToCachedArgumentValue =
                         new FlagToCachedArgumentValueDictionary();

            public BuilderCachedArguments(BuilderMetadata builderMetadata)
            {
                foreach (var buildStepMetadata in builderMetadata.BuildSteps)
                {
                    foreach (var buildStepArgumentMetadata in buildStepMetadata.ParameterList)
                    {
                        if (!_flagToCachedArgumentValue.ContainsKey(buildStepArgumentMetadata.Flag))
                        {
                            _flagToCachedArgumentValue.Add
                            (
                                buildStepArgumentMetadata.Flag,
                                new CachedArgumentValue
                                {
                                    Type = buildStepArgumentMetadata.Type
                                }
                            );
                        }
                    }
                }
            }

            public void RestoreValues(FlagToCachedArgumentValueDictionary sourceDictionary)
            {
                foreach (var sourceFlagValuePair in sourceDictionary)
                {
                    CachedArgumentValue value = Get(sourceFlagValuePair.Key);
                    if (value != null)
                    {
                        value.CopyFrom(sourceFlagValuePair.Value);
                    }
                }
            }

            public CachedArgumentValue Get(string flag)
            {
                return _flagToCachedArgumentValue.ContainsKey(flag) ? _flagToCachedArgumentValue[flag] : null;
            }

            public Dictionary<string, CachedArgumentValue> GetAll()
            {
                return _flagToCachedArgumentValue;
            }
        }


        private bool         _isCreationMethodsInEditorAssemblyListed;
        private MethodInfo[] _builderCreationMethods;

        private class BuilderCreationMethodEditorState
        {
            public bool                   IsFoldoutOpen;
            public BuilderMetadata        BuilderMetadata;
            public BuilderCachedArguments Arguments;
        }

        private readonly Dictionary<MethodInfo, BuilderCreationMethodEditorState> _creationMethodToEditorStateMapping =
            new Dictionary<MethodInfo, BuilderCreationMethodEditorState>();

        [MenuItem("Rayark/Mimiron Lite/Editor Builder")]
        private static void Init()
        {
            var editorBuilder = (EditorBuilder) GetWindow(typeof(EditorBuilder));
            editorBuilder.Show();
        }

#if UNITY_2017_1_OR_NEWER
        private void OnEnable()
        {
            AssemblyReloadEvents.beforeAssemblyReload += _OnBeforeAssemblyReload;
            AssemblyReloadEvents.afterAssemblyReload += _OnAfterAssemblyReload;
        }

        private void OnDisable()
        {
            AssemblyReloadEvents.beforeAssemblyReload -= _OnBeforeAssemblyReload;
            AssemblyReloadEvents.afterAssemblyReload -= _OnAfterAssemblyReload;
            _SerializeToEditorPrefs();
        }

        private void _OnBeforeAssemblyReload()
        {
            _SerializeToEditorPrefs();
        }

        private void _OnAfterAssemblyReload()
        {
            _ListAllBuilderCreationFunctions();
            _DeserializeFromEditorPrefs();
        }
#else
        private void OnDisable()
        {
            _SerializeToEditorPrefs();
        }
#endif

        private void OnGUI()
        {
            if (!_isCreationMethodsInEditorAssemblyListed)
            {
                _ListAllBuilderCreationFunctions();
                _DeserializeFromEditorPrefs();
            }

            foreach (MethodInfo creationMethod in _builderCreationMethods)
            {
                BuilderCreationMethodEditorState state = _creationMethodToEditorStateMapping[creationMethod];

                BuilderMetadata        builderMetadata = state.BuilderMetadata;
                BuilderCachedArguments cachedValueSet  = state.Arguments;

                string foldoutDisplay = !string.IsNullOrEmpty(builderMetadata.Description)
                                            ? builderMetadata.Description
                                            : creationMethod.Name;
                state.IsFoldoutOpen = EditorGUILayout.Foldout(state.IsFoldoutOpen, foldoutDisplay);
                EditorGUI.indentLevel++;
                {
                    if (state.IsFoldoutOpen)
                    {
                        foreach (BuildStepMetadata buildStepMetadata in builderMetadata.BuildSteps)
                        {
                            EditorGUILayout.LabelField(buildStepMetadata.Description);
                            EditorGUI.indentLevel++;
                            {
                                foreach (ParameterMetadata arg in buildStepMetadata.ParameterList)
                                {
                                    CachedArgumentValue cachedValueRef = cachedValueSet.Get(arg.Flag);
                                    switch (arg.Type)
                                    {
                                        case ParameterType.String:
                                            cachedValueRef.CachedStringValue = EditorGUILayout.TextField
                                            (
                                                CommandLineArguments.PrependFlagDash(arg.Flag),
                                                cachedValueRef.CachedStringValue
                                            );
                                            break;
                                        case ParameterType.Bool:
                                            cachedValueRef.CachedBoolValue = EditorGUILayout.Toggle
                                            (
                                                CommandLineArguments.PrependFlagDash(arg.Flag),
                                                cachedValueRef.CachedBoolValue
                                            );
                                            break;
                                        case ParameterType.Float:
                                            cachedValueRef.CachedFloatValue = EditorGUILayout.FloatField
                                            (
                                                CommandLineArguments.PrependFlagDash(arg.Flag),
                                                cachedValueRef.CachedFloatValue
                                            );
                                            break;
                                        case ParameterType.Int:
                                            cachedValueRef.CachedIntValue = EditorGUILayout.IntField
                                            (
                                                CommandLineArguments.PrependFlagDash(arg.Flag), 
                                                cachedValueRef.CachedIntValue
                                            );
                                            break;
                                    }
                                }
                            }
                            EditorGUI.indentLevel--;
                        }

                        if (GUILayout.Button("Build"))
                        {
                            Builder builder   = _CreateBuilderFromCreationMethod(creationMethod);
                            var     arguments = new List<string>();

                            foreach (KeyValuePair<string, CachedArgumentValue> cachedArgumentValuePair in cachedValueSet
                                .GetAll())
                            {
                                string flag =
                                    CommandLineArguments.PrependFlagDash(cachedArgumentValuePair.Key);
                                CachedArgumentValue cachedArgumentValue = cachedArgumentValuePair.Value;
                                arguments.Add(flag);

                                switch (cachedArgumentValue.Type)
                                {
                                    case ParameterType.String:
                                        arguments.Add(cachedArgumentValue.CachedStringValue);
                                        break;
                                    case ParameterType.Bool:
                                        arguments.Add(cachedArgumentValue.CachedBoolValue.ToString());
                                        break;
                                    case ParameterType.Float:
                                        arguments.Add(cachedArgumentValue.CachedFloatValue.ToString());
                                        break;
                                    case ParameterType.Int:
                                        arguments.Add(cachedArgumentValue.CachedIntValue.ToString());
                                        break;
                                }
                            }

                            var commandLineArguments = new CommandLineArguments(arguments.ToArray());
                            builder.Build(commandLineArguments);
                        }
                    }
                }
                EditorGUI.indentLevel--;
            }
        }

        private void _ListAllBuilderCreationFunctions()
        {
            _builderCreationMethods = _GetAllBuilderCreationMethods();

            foreach (MethodInfo creationMethod in _builderCreationMethods)
            {
                Builder builder = _CreateBuilderFromCreationMethod(creationMethod);
                if (builder == null)
                {
                    continue;
                }

                BuilderMetadata builderMetadata = builder.GetMetadata();
                var state = new BuilderCreationMethodEditorState
                            {
                                IsFoldoutOpen = false,
                                BuilderMetadata = builderMetadata,
                                Arguments =
                                    new BuilderCachedArguments(builderMetadata)
                            };
                _creationMethodToEditorStateMapping.Add(creationMethod, state);
            }

            _isCreationMethodsInEditorAssemblyListed = true;
        }

        private static Builder _CreateBuilderFromCreationMethod(MethodInfo creationMethod)
        {
            return creationMethod.Invoke(null, null) as Builder;
        }

        private static MethodInfo[] _GetAllBuilderCreationMethods()
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                            .SelectMany(assembly => assembly.GetTypes())
                            .SelectMany(type => type.GetMethods(BindingFlags.Public |
                                                                BindingFlags.NonPublic |
                                                                BindingFlags.Static))
                            .Where(_IsValidBuilderCreationMethod)
                            .ToArray();
        }

        private static bool _IsValidBuilderCreationMethod(MethodInfo methodInfo)
        {
            bool hasBuilderCreationAttribute = 0 < methodInfo.GetCustomAttributes(typeof(BuilderCreationAttribute), false).Length;
            bool isReturnTypeBuilder = methodInfo.ReturnType == typeof(Builder);
            bool hasNoParameter = methodInfo.GetParameters().Length == 0;

            return hasBuilderCreationAttribute && isReturnTypeBuilder && hasNoParameter;
        }

        [Serializable]
        public class SerializableEditorState
        {
            [Serializable]
            public class SerializableBuilderState
            {
                public string                             MethodPath;
                public bool                               IsFoldoutOpen;
                public List<SerializableFlagArgumentPair> KeyedArguments;
            }

            [Serializable]
            public class SerializableFlagArgumentPair
            {
                public string              Flag;
                public CachedArgumentValue Value;
            }

            public List<SerializableBuilderState> SerializedBuilderStateList = new List<SerializableBuilderState>();
        }

        private void _SerializeToEditorPrefs()
        {
#if JSON_UTILITY_AVAILABLE
            var serializableEditorState = new SerializableEditorState();

            foreach (var creationMethodAndStatePair in _creationMethodToEditorStateMapping)
            {
                MethodInfo method         = creationMethodAndStatePair.Key;
                string     methodFullPath = _GetMethodFullPath(method);

                BuilderCreationMethodEditorState state = creationMethodAndStatePair.Value;
                serializableEditorState.SerializedBuilderStateList.Add(
                    new SerializableEditorState.SerializableBuilderState
                    {
                        MethodPath =
                            methodFullPath,
                        IsFoldoutOpen =
                            state
                                .IsFoldoutOpen,
                        KeyedArguments =
                            state
                                .Arguments
                                .GetAll()
                                .Select
                                (
                                    kvp =>
                                        new
                                        SerializableEditorState
                                        .
                                        SerializableFlagArgumentPair
                                        {
                                            Flag
                                                = kvp
                                                    .Key,
                                            Value
                                                = kvp
                                                    .Value
                                        }
                                ).ToList()
                    });
            }

            string json = JsonUtility.ToJson(serializableEditorState);
            EditorPrefs.SetString(_GetProjectEditorPrefsKey(), json);
#endif
        }

        private void _DeserializeFromEditorPrefs()
        {
#if JSON_UTILITY_AVAILABLE
            string json                    = EditorPrefs.GetString(_GetProjectEditorPrefsKey());
            var    deserializedEditorState = JsonUtility.FromJson<SerializableEditorState>(json);
            if (deserializedEditorState == null)
            {
                return;
            }

            foreach (var creationMethodAndStatePair in _creationMethodToEditorStateMapping)
            {
                MethodInfo method         = creationMethodAndStatePair.Key;
                string     methodFullPath = _GetMethodFullPath(method);

                SerializableEditorState.SerializableBuilderState deserializedBuilderState =
                    deserializedEditorState.SerializedBuilderStateList.FirstOrDefault(status =>
                                                                                          status.MethodPath ==
                                                                                          methodFullPath);

                if (deserializedBuilderState != null)
                {
                    var builderState = _creationMethodToEditorStateMapping[method];
                    builderState.IsFoldoutOpen = deserializedBuilderState.IsFoldoutOpen;
                    var deserializedArgument = deserializedBuilderState.KeyedArguments;
                    if (deserializedArgument != null)
                    {
                        var newFlagToArgumentValueMapping = new Dictionary<string, CachedArgumentValue>();
                        deserializedArgument.ForEach(s => newFlagToArgumentValueMapping.Add(s.Flag, s.Value));
                        builderState.Arguments.RestoreValues(newFlagToArgumentValueMapping);
                    }
                }
            }
#endif
        }

        private string _GetProjectEditorPrefsKey()
        {
            string productName = UnityEditor.PlayerSettings.productName;
            return "mimiron-lite-" + productName;
        }

        private string _GetMethodFullPath(MethodInfo method)
        {
            return string.Format("{0}.{1}", method.DeclaringType.FullName, method.Name);
        }
    }

    /// <summary>
    /// Attribute for tagging static functions return a <see cref="Builder"/> is intended to be shown in Unity editor UI
    /// </summary>
    public class BuilderCreationAttribute : Attribute { }
}