using System.Linq;
using System.Collections.Generic;


namespace Rayark.MimironLite
{
    /// <summary>
    /// Unity C# define symbol collection
    /// </summary>
    public class DefineSymbolCollection
    {
        private readonly List<DefineSymbol> _symbols = new List<DefineSymbol>();

        /// <summary>
        /// Add define symbol to collection
        /// </summary>
        /// <param name="symbol">Define symbol to add</param>
        public void Add(DefineSymbol symbol)
        {
            _symbols.Add(symbol);
        }

        /// <summary>
        /// Remove define symbol from collection
        /// </summary>
        /// <param name="symbol">Define symbol to remove</param>
        public void Remove(DefineSymbol symbol)
        {
            _symbols.RemoveAll(s => s == symbol);
        }

        /// <summary>
        /// Create collection from Unity PlayerSettings serialized define symbols
        /// </summary>
        /// <param name="defineSymbolsString">Symbols string from PlayerSettings</param>
        /// <returns>A define symbol collection from symbols string</returns>
        public static DefineSymbolCollection FromUnityDefineSymbolsString(string defineSymbolsString)
        {
            var parsedSymbols = defineSymbolsString.Split(';')
                                                   .Where(_IsNotNullOrEmpty)
                                                   .Distinct()
                                                   .Select(s => new DefineSymbol(s));
            var collection = new DefineSymbolCollection();
            collection._symbols.AddRange(parsedSymbols);
            return collection;
        }

        private static bool _IsNotNullOrEmpty(string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Serialized a collection to Unity PlayerSettings format
        /// </summary>
        /// <param name="collection">Collection to serialize</param>
        /// <returns>Serialized define symbols, separated with semicolon</returns>
        public static string ToUnityDefineSymbolsString(DefineSymbolCollection collection)
        {
            return string.Join(";", collection._symbols.Select(ds => ds.Symbol).ToArray());
        }
    }
}