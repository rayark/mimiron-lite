using System;


namespace Rayark.MimironLite
{
    /// <summary>
    /// Struct representing single Unity C# define symbol
    /// </summary>
    public struct DefineSymbol
    {
        private readonly string _symbol;
        /// <value>Define symbol literal text</value>
        public string Symbol { get { return _symbol; } }

        /// <summary>
        /// Constructor of DefineSymbol
        /// </summary>
        /// <param name="symbol">Define symbol literal text</param>
        /// <exception cref="ArgumentException">Thrown when literal text contains semicolon or empty,
        /// since semicolon is Unity define symbol separator</exception>
        public DefineSymbol(string symbol)
        {
            if (string.IsNullOrEmpty(symbol))
            {
                throw new ArgumentException("Define symbol cannot be null or empty");
            }

            if (symbol.Contains(";"))
            {
                throw new ArgumentException("Define symbol cannot contain semicolon ';'");
            }

            _symbol = symbol;
        }


        /// <summary>
        /// Implicit conversion from string to DefineSymbol
        /// </summary>
        /// <param name="symbolLiteral">String to convert</param>
        /// <returns>Converted DefineSymbol</returns>
        public static implicit operator DefineSymbol(string symbolLiteral)
        {
            return new DefineSymbol(symbolLiteral);
        }

        /// <summary>
        /// Implicit conversion from DefineSymbol to string
        /// </summary>
        /// <param name="defineSymbol">DefineSymbol to convert</param>
        /// <returns>Converted string</returns>
        public static implicit operator string(DefineSymbol defineSymbol)
        {
            return defineSymbol._symbol;
        }
    }
}