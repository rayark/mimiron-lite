using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

using UnityEngine;


namespace Rayark.MimironLite
{
    using Rayark.MimironLite.Metadata; 

    /// <summary>
    /// Helper class for auto-filling build step arguments from CommandLineArguments
    /// </summary>
    public class ParameterUtilities
    {
        private const BindingFlags PUBLIC_INSTANCE_FLAG  = BindingFlags.Instance | BindingFlags.Public;


        /// <summary>
        /// Parse parameters from CommandLineArguments and write into IBuildStepParameter with reflection
        /// </summary>
        /// <param name="arguments">Command line arguments wrapper to read raw value from</param>
        /// <param name="buildStepParameter">Data structure to write</param>
        public static void ParseParameters(CommandLineArguments arguments, IBuildStepParameter buildStepParameter)
        {
            Type buildStepParameterType = buildStepParameter.GetType();
            FieldInfo[] parameterFieldInfos = buildStepParameterType.GetFields(PUBLIC_INSTANCE_FLAG);

            foreach (FieldInfo parameterFieldInfo in parameterFieldInfos)
            {
                var flagAttribute = parameterFieldInfo.GetCustomAttributes(inherit: false)
                                                      .FirstOrDefault(attr => attr is CommandlineFlagAttribute)
                                                                                   as CommandlineFlagAttribute;

                if (flagAttribute == null)
                {
                    string message = string.Format("Field {0} on build step parameter does not have flag attribute",
                                                   parameterFieldInfo.Name);
                    Debug.LogWarning(message);
                    continue;
                }

                if (parameterFieldInfo.FieldType == typeof(string))
                {
                    var retrievedValue = arguments.RetrieveArgument(flagAttribute.Flag);
                    parameterFieldInfo.SetValue(buildStepParameter, retrievedValue);
                }
                else if (parameterFieldInfo.FieldType == typeof(int))
                {
                    var retrievedValue = arguments.RetrieveArgumentAsInt(flagAttribute.Flag);
                    parameterFieldInfo.SetValue(buildStepParameter, retrievedValue);
                }
                else if (parameterFieldInfo.FieldType == typeof(float))
                {
                    var retrievedValue = arguments.RetrieveArgumentAsFloat(flagAttribute.Flag);
                    parameterFieldInfo.SetValue(buildStepParameter, retrievedValue);
                }
                else if (parameterFieldInfo.FieldType == typeof(bool))
                {
                    var retrievedValue = arguments.RetrieveArgumentAsBool(flagAttribute.Flag);
                    parameterFieldInfo.SetValue(buildStepParameter, retrievedValue);
                }
                else
                {
                    string message = string.Format(
                        "Field {0} on build step parameter is not a supported type.\nSupported types are string, int, float and bool", 
                        parameterFieldInfo.Name);
                    Debug.LogWarning(message);
                }
            }
        }

        /// <summary>
        /// Dump all build step parameter metadata
        /// </summary>
        /// <param name="buildStepParameterType">Parameter type to dump</param>
        /// <returns>Parameter metadata list</returns>
        public static List<ParameterMetadata> GetMetadataList(Type buildStepParameterType)
        {
            if (!typeof(IBuildStepParameter).IsAssignableFrom(buildStepParameterType))
            {
                return new List<ParameterMetadata>();
            }

            var metadataList = new List<ParameterMetadata>();

            FieldInfo[] parameterFieldInfos = buildStepParameterType.GetFields(PUBLIC_INSTANCE_FLAG);
            foreach (FieldInfo parameterFieldInfo in parameterFieldInfos)
            {
                metadataList.Add(new ParameterMetadata(parameterFieldInfo));
            }

            return metadataList;
        }
    }
}