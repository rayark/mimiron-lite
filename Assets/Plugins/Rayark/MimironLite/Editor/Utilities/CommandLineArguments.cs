using System;
using System.Linq;


namespace Rayark.MimironLite
{
    /// <summary>
    /// Exception for cannot locate argument with certain flag
    /// </summary>
    public class MissingArgumentException : Exception
    {
        /// <value>
        /// The flag which is missing
        /// </value>
        public string Flag { get; private set; }

        /// <param name="flag">The flag which cannot be found in CommandLineArguments</param>
        public MissingArgumentException(string flag)
        {
            Flag = flag;
        }
    }

    /// <summary>
    /// Exception for invalid argument format
    /// </summary>
    public class InvalidFormatException : Exception
    {
        /// <value>
        /// The flag which identified the argument in question
        /// </value>
        public string Flag { get; private set; }

        /// <value>
        /// Raw string which CommandLineArguments failed to parse 
        /// </value>
        public string GivenString { get; private set; }

        /// <param name="flag">The flag</param>
        /// <param name="givenString">Raw string</param>
        public InvalidFormatException(string flag, string givenString)
        {
            Flag = flag;
            GivenString = givenString;
        }
    }

    /// <summary>
    /// Command line arguments wrapper with helper methods to extract values
    /// </summary>
    public class CommandLineArguments
    {
        private readonly string[] _args;

        /// <param name="args">Pass return value of Environment.GetCommandLineArgs() here</param>
        public CommandLineArguments(string[] args)
        {
            _args = args;
        }

        /// <summary>
        /// Is command line arguments containing certain flag
        /// </summary>
        /// <param name="flag">Command line flag to check</param>
        /// <returns>Is the flag existing in command line arguments</returns>
        public bool HasArgument(string flag)
        {
            flag = PrependFlagDash(flag);

            return _args.Contains(flag);
        }

        /// <summary>
        /// Ensure flag string is prepended with dash '-'
        /// </summary>
        /// <param name="flag">Flag to ensure</param>
        /// <returns>A flag string starts with '-'</returns>
        public static string PrependFlagDash(string flag)
        {
            return !_IsFormattedAsFlag(flag) ? "-" + flag : flag;
        }

        private static bool _IsFormattedAsFlag(string supposedFlag)
        {
            return supposedFlag != null && supposedFlag.StartsWith("-");
        }


        /// <summary>
        /// Retrieve argument after certain flag as raw string
        /// </summary>
        /// <param name="flag">Flag to retrieve</param>
        /// <returns>Argument after the flag as string</returns>
        /// <exception cref="MissingArgumentException">Thrown when flag does not exist</exception>
        public string RetrieveArgument(string flag)
        {
            flag = PrependFlagDash(flag);

            // Argument = Commandline argument next to flag
            string argumentValue = _args.SkipWhile(arg => !string.Equals(arg, flag))
                                        .Skip(1)
                                        .FirstOrDefault();

            // Default value of string (Reference type) is null
            // If argument next to flag is another flag, then the value is omitted
            if (argumentValue == null || _IsFormattedAsFlag(argumentValue))
            {
                throw new MissingArgumentException(flag);
            }

            return argumentValue;
        }

        /// <summary>
        /// Retrieve argument after certain flag as bool
        /// "true" as boolean true, and "false" as boolean false, matching ignores cases 
        /// </summary>
        /// <param name="flag">Flag to retrieve</param>
        /// <returns>Argument after the flag as bool</returns>
        /// <exception cref="MissingArgumentException">Thrown when flag does not exist</exception>
        /// <exception cref="InvalidFormatException">Thrown when argument is neither "true" nor "false"</exception>
        public bool RetrieveArgumentAsBool(string flag)
        {
            string argumentValue = RetrieveArgument(flag);

            if (string.Equals(argumentValue, "true", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            if (string.Equals(argumentValue, "false", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            throw new InvalidFormatException(flag, argumentValue);
        }

        /// <summary>
        /// Retrieve argument after certain flag as int
        /// </summary>
        /// <param name="flag">Flag to retrieve</param>
        /// <returns>Argument after the flag as int</returns>
        /// <exception cref="MissingArgumentException">Thrown when flag does not exist</exception>
        /// <exception cref="InvalidFormatException">Thrown when argument cannot be parsed to int</exception>
        public int RetrieveArgumentAsInt(string flag)
        {
            string argumentValue = RetrieveArgument(flag);

            try
            {
                return int.Parse(argumentValue);
            }
            catch
            {
                throw new InvalidFormatException(flag, argumentValue);
            }
        }

        /// <summary>
        /// Retrieve argument after certain flag as float
        /// </summary>
        /// <param name="flag">Flag to retrieve</param>
        /// <returns>Argument after the flag as float</returns>
        /// <exception cref="MissingArgumentException">Thrown when flag does not exist</exception>
        /// <exception cref="InvalidFormatException">Thrown when argument cannot be parsed to float</exception>
        public float RetrieveArgumentAsFloat(string flag)
        {
            string argumentValue = RetrieveArgument(flag);

            try
            {
                return float.Parse(argumentValue);
            }
            catch
            {
                throw new InvalidFormatException(flag, argumentValue);
            }
        }

        /// <summary>
        /// Join all arguments into a string
        /// </summary>
        /// <returns>Arguments separated with white space</returns>
        public override string ToString()
        {
            return string.Join(" ", _args);
        }
    }
}