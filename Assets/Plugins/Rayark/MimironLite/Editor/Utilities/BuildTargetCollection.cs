using System.Collections.Generic;

using UnityEditor;


namespace Rayark.MimironLite
{
    /// <summary>
    /// Data structure for representing a collection of BuildTarget or any BuildTarget
    /// </summary>
    public class BuildTargetCollection
    {
        /// <value> Static instance of BuildTargetCollection representing any platform</value>
        public static BuildTargetCollection Any         = new BuildTargetCollection();
        /// <value> Static instance of BuildTargetCollection representing Android only</value>
        public static BuildTargetCollection AndroidOnly = new BuildTargetCollection(BuildTarget.Android);
        /// <value> Static instance of BuildTargetCollection representing iOS only</value>
        public static BuildTargetCollection iOSOnly     = new BuildTargetCollection(BuildTarget.iOS);

        private readonly bool              _isAny;
        private readonly List<BuildTarget> _targetList = new List<BuildTarget>();

        private BuildTargetCollection()
        {
            _isAny = true;
        }

        /// <summary>
        /// Create BuildTargetCollection with single BuildTarget
        /// </summary>
        /// <param name="buildTarget">Single BuildTarget to build with</param>
        public BuildTargetCollection(BuildTarget buildTarget)
        {
            _targetList.Add(buildTarget);
        }

        /// <summary>
        /// Create BuildTargetCollection with multiple BuildTarget
        /// </summary>
        /// <param name="buildTargets">Multiple BuildTarget to build with</param>
        public BuildTargetCollection(IEnumerable<BuildTarget> buildTargets)
        {
            _targetList.AddRange(buildTargets);
        }

        /// <summary>
        /// Check if BuildTargetCollection contains a BuildTarget
        /// </summary>
        /// <param name="container">BuildTargetCollection to test</param>
        /// <param name="item">BuildTarget to test</param>
        /// <returns>Is BuildTargetCollection containing BuildTarget</returns>
        public static bool Contains(BuildTargetCollection container, BuildTarget item)
        {
            return container._isAny || container._targetList.Contains(item);
        }
    }

    /// <summary>
    /// Host of BuildTargetCollection extension methods
    /// </summary>
    public static class BuildTargetCollectionExtenstion
    {
        /// <summary>
        /// Extension method to make static BuildTargetCollection.Contains callable like method 
        /// </summary>
        /// <param name="container">BuildTargetCollection to test</param>
        /// <param name="item">BuildTarget to test</param>
        /// <returns>Is BuildTargetCollection containing BuildTarget</returns>
        public static bool Contains(this BuildTargetCollection container, BuildTarget item)
        {
            return BuildTargetCollection.Contains(container, item);
        }
    }
}