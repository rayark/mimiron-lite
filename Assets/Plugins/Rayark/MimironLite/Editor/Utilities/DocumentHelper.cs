using System;
using System.Reflection;

namespace Rayark.MimironLite.Documentation
{
    public class DocumentHelper
    {
        public static void SyncSolution()
        {
            var T = Type.GetType("UnityEditor.SyncVS,UnityEditor");
            var syncSolution = T.GetMethod("SyncSolution", BindingFlags.Public | BindingFlags.Static);
            syncSolution.Invoke(null, null);
        }
    }
}