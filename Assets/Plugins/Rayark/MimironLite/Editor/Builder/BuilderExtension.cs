#if UNITY_5 || UNITY_5_3_OR_NEWER
#define UNITY_5_OR_NEWER
#endif

#if UNITY_5_1_1 || UNITY_5_1_2 || UNITY_5_1_3 || UNITY_5_1_4 || UNITY_5_1_5 || UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_1_1_OR_NEWER
#endif

#if UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_2_OR_NEWER
#endif


#if UNITY_5_OR_NEWER
#define ANDROID_BUILD_SUBTARGET_RENAMED_TO_MOBILE_TEXTURE_SUBTARGET
#define EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE
#endif

#if UNITY_5_1_1_OR_NEWER
#define GPU_SKINNING_SETTER_AVAILABLE
#endif

#if UNITY_5_2_OR_NEWER
#define IOS_BUILD_NUMBER_AVAILABLE
#endif

#if UNITY_5_5_OR_NEWER
#define ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
#endif

#if UNITY_5_6_OR_NEWER
#define BUNDLE_IDENTIFIER_RENAMED_TO_APPLICATION_IDENTIFIER
#endif

#if UNITY_2018_2_OR_NEWER
#define BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
#endif


namespace Rayark.MimironLite.Build
{
    using Rayark.MimironLite.BuildStep;
    using Rayark.MimironLite.BuildStep.Android;
    using Rayark.MimironLite.BuildStep.iOS;

    /// <summary>
    /// Extension methods for adding build steps in fluent style
    /// </summary>
    // Intentionally made partial for extension
    public static partial class BuilderExtension
    {
        private static Builder _AppendBuildStep(Builder builder, IBuildStep buildStep)
        {
            builder.AddChildStep(buildStep);
            return builder;
        }

        /// <summary>
        /// Extension method to add SetBuildTargetPathBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetBuildTargetPathStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetBuildTargetPathBuildStep());
        }

        /// <summary>
        /// Extension method to add SetDevelopmentBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetDevelopmentStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetDevelopmentBuildStep());
        }

        /// <summary>
        /// Extension method to add SetBundleVersionBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetBundleVersionStep(this Builder builder) 
        {
            return _AppendBuildStep(builder, new SetBundleVersionBuildStep());
        }

        /// <summary>
        /// Extension method to add SetBundleIdentifierBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetBundleIdentifierStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetBundleIdentifierBuildStep());
        }

        
#if GPU_SKINNING_SETTER_AVAILABLE
        /// <summary>
        /// Extension method to add SetGpuSkinningBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetGpuSkinningStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetGpuSkinningBuildStep());
        }
#endif

        /// <summary>
        /// Extension method to add SetDefineSymbolBuildStep to a Builder 
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <param name="flag">Flag that trigger define symbol add/remove</param>
        /// <param name="defineSymbol">Define symbol to be added/removed</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetDefineSymbolStep(this Builder builder, string flag, string defineSymbol)
        {
            return _AppendBuildStep(builder, new SetDefineSymbolBuildStep(flag, defineSymbol));
        }


        #region Android
        /// <summary>
        /// Extension method to add SetAndroidVersionNameBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddAndroidVersionNameStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetAndroidVersionNameBuildStep());
        }

        /// <summary>
        /// Extension method to add SetAndroidVersionCodeBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddAndroidVersionCodeStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetAndroidVersionCodeBuildStep());
        }

        /// <summary>
        /// Extension method to add SetAndroidKeyStoreBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetAndroidKeyStoreStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetAndroidKeyStoreBuildStep());
        }

        /// <summary>
        /// Extension method to add SetAndroidSdkRootPathBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetAndroidSdkRootPathStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetAndroidSdkRootPathBuildStep());
        }

        /// <summary>
        /// Extension method to add SetAndroidNdkRootPathBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetAndroidNdkRootPathStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetAndroidNdkRootPathBuildStep());
        }

        /// <summary>
        /// Extension method to add SetAndroidApkExpansionFilesBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetAndroidApkExpansionFilesStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetAndroidApkExpansionFilesBuildStep());
        }

#if EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE && ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
        /// <summary>
        /// Extension method to add SetIsExportingGradleProjectBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetIsExportingGradleProjectStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetIsExportingGradleProjectBuildStep());
        }
#endif

#if BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
        /// <summary>
        /// Extension method to add SetIsBuildingApkPerCpuArchitectureBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetIsBuildingApkPerCpuArchitectureStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetIsBuildingApkPerCpuArchitectureBuildStep());
        }
#endif
        #endregion


        #region iOS
        /// <summary>
        /// Extension method to add SetiOSVersionStringBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetiOSVersionStringStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetiOSVersionStringBuildStep());
        }

#if IOS_BUILD_NUMBER_AVAILABLE
        /// <summary>
        /// Extension method to add SetiOSBuildStringBuildStep to a Builder
        /// </summary>
        /// <param name="builder">Builder to add build step</param>
        /// <returns>Same builder for fluent style</returns>
        public static Builder AddSetiOSBuildStringStep(this Builder builder)
        {
            return _AppendBuildStep(builder, new SetiOSBuildStringBuildStep());
        }
#endif
        #endregion
    }
}