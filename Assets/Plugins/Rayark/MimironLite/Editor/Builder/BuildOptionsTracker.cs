using System.Collections.Generic;

using UnityEditor;


namespace Rayark.MimironLite.Build
{
    /// <summary>
    /// Helper class for tracking all Unity build option flags and combine them on calling Unity build
    /// </summary>
    public class BuildOptionsTracker
    {
        private readonly List<BuildOptions> _buildOptionsToAdd    = new List<BuildOptions>();
        private readonly List<BuildOptions> _buildOptionsToRemove = new List<BuildOptions>();

        /// <summary>
        /// Add a BuildOptions flag
        /// </summary>
        /// <param name="buildOptions">BuildOptions flag to add</param>
        public void Add(BuildOptions buildOptions)
        {
            _buildOptionsToAdd.Add(buildOptions);
        }

        /// <summary>
        /// Remove a BuildOptions flag
        /// </summary>
        /// <param name="buildOptions">BuildOptions flag to remove</param>
        public void Remove(BuildOptions buildOptions)
        {
            _buildOptionsToRemove.Add(buildOptions);
        }

        /// <summary>
        /// Get all BuildOptions flag combined value
        /// </summary>
        /// <returns>BuildOptions will all flags</returns>
        public BuildOptions GetFinalBuildOptions()
        {
            var resultBuildOptions = BuildOptions.None;
            _buildOptionsToAdd.ForEach(addOptions =>
                                       resultBuildOptions = _AddBuildOptions(resultBuildOptions, addOptions));
            _buildOptionsToRemove.ForEach(removeOptions =>
                                          resultBuildOptions = _RemoveBuildOptions(resultBuildOptions, removeOptions));
            return resultBuildOptions;
        }

        private static BuildOptions _AddBuildOptions(BuildOptions original, BuildOptions flag)
        {
            return original | flag;
        }

        private static BuildOptions _RemoveBuildOptions(BuildOptions original, BuildOptions flag)
        {
            return original & ~flag;
        }
    }
}