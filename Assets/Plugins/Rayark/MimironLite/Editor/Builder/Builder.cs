#if UNITY_2018_1_OR_NEWER
#define BUILD_REPORT_API
#endif

using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
#if BUILD_REPORT_API
using UnityEditor.Build.Reporting;
#endif


namespace Rayark.MimironLite.Build
{
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.BuildStep;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// Exception thrown when builder cannot execute a build step
    /// </summary>
    public class BuildStepFailedException : Exception { }

    /// <summary>
    /// Build executor
    /// </summary>
    public class Builder
    {
        /// <summary>
        /// Static helper function creates a Builder for any platform
        /// </summary>
        /// <param name="description">Builder description</param>
        /// <returns>Built Builder</returns>
        public static Builder Create(string description = null)
        {
            return new Builder(description);
        }

        /// <summary>
        /// Static helper function creates a Builder instance with applicable BuildTarget set
        /// </summary>
        /// <param name="buildTarget">Builder target platform</param>
        /// <param name="description">Builder description</param>
        /// <returns>Built Builder</returns>
        public static Builder Create(BuildTarget buildTarget, string description = null)
        {
            return new Builder(buildTarget, description);
        }


        private readonly bool             _isAnyPlatformBuilder;
        private readonly BuildTarget      _buildTargetPlatform;
        private readonly List<IBuildStep> _childrenSteps = new List<IBuildStep>();
        private readonly string           _description;

        /// <summary>
        /// Constructor for platform agnostic Builder, the Builder will run in current Unity editor target platform
        /// </summary>
        /// <param name="description">Builder description, used in editor UI and command line help</param>
        public Builder(string description)
        {
            _isAnyPlatformBuilder = true;
            _description          = description;
        }

        /// <summary>
        /// Constructor for platform specific Builder
        /// </summary>
        /// <param name="buildTargetPlatform">Builder target platform</param>
        /// <param name="description">Builder description, used in editor UI and command line help</param>
        public Builder(BuildTarget buildTargetPlatform, string description)
        {
            _buildTargetPlatform = buildTargetPlatform;
            _description         = description;
        }

        /// <summary>
        /// Add build step to Builder, will be executed in added order
        /// </summary>
        /// <param name="step">Build step to add</param>
        public void AddChildStep(IBuildStep step)
        {
            if
            (
                !_isAnyPlatformBuilder &&
                !step.GetApplicableBuildTargets().Contains(_buildTargetPlatform)
            )
            {
                Debug.LogWarning("Adding a build step that does not match target platform of builder, ignored");
                return;
            }

            _childrenSteps.Add(step);
        }

        /// <summary>
        /// Run all build step with command line arguments
        /// </summary>
        /// <param name="arguments">Wrapped command line arguments</param>
        /// <exception cref="BuildStepFailedException">Thrown when any child build step throws an exception</exception>
        /// <exception cref="BuildPipelineFailedException">Thrown when Unity Build Pipeline failed to execute</exception>
        public BuildReport Build(CommandLineArguments arguments)
        {
            _PrintHelp();
            _PrintCommandlineArguments(arguments);

            var targetPlatform = _GetBuildTargetPlatform();

            var playerSettingsManipulator = new PlayerSettingsManipulator(targetPlatform);
            var buildTargetPathTracker    = new BuildTargetPathTracker();
            var buildOptionsTracker       = new BuildOptionsTracker();

            foreach (var childStep in _childrenSteps)
            {
                try
                {
                    childStep.Execute
                    (
                        arguments,
                        playerSettingsManipulator,
                        buildTargetPathTracker,
                        buildOptionsTracker
                    );
                }
                catch (MissingArgumentException ex)
                {
                    var stringBuilder = new StringBuilder();
                    stringBuilder.AppendFormat("On executing BuildStep typed: {0}\n", childStep.GetType());
                    stringBuilder.AppendFormat("Cannot locate argument correspond to flag: [{0}]\n", ex.Flag);
                    Debug.LogError(stringBuilder);

                    throw new BuildStepFailedException();
                }
                catch (InvalidFormatException ex)
                {
                    var stringBuilder = new StringBuilder();
                    stringBuilder.AppendFormat("On executing BuildStep typed: {0}\n", childStep.GetType());
                    stringBuilder.AppendFormat("Invalid formatted argument: [{0}] encountered when parsing flag: [{1}]", ex.GivenString, ex.Flag);
                    Debug.LogError(stringBuilder);

                    throw new BuildStepFailedException();
                }
            }

            var allActiveScenes   = _GetAllActivatedSceneNames();
            var finalBuildOptions = buildOptionsTracker.GetFinalBuildOptions();

#if BUILD_REPORT_API
            var buildPlayerOptions = new BuildPlayerOptions
            {
                scenes = allActiveScenes,
                locationPathName = buildTargetPathTracker.BuildTargetPath,
                target = targetPlatform, 
                targetGroup = BuildPipeline.GetBuildTargetGroup(targetPlatform),
                options = finalBuildOptions
            };
            
            BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);

            if (buildReport.summary.result != BuildResult.Succeeded)
            {
                Debug.LogError("Unity BuildPipeline failed");
            }
            return buildReport;
#else
            var startTime = DateTime.Now;
            var buildReport = new BuildReport
                              {
                                  Options = finalBuildOptions,
                                  Platform = targetPlatform,
                                  PlatformGroup = playerSettingsManipulator.TargetGroup,
                                  OutputPath = buildTargetPathTracker.BuildTargetPath,
                                  _buildStartTimeTicks = startTime.Ticks,
                                  BuildType = BuildType.Player,
                                  ErrorMessage = string.Empty
                              };
            string errorMessage = BuildPipeline.BuildPlayer
            (
                allActiveScenes,
                buildTargetPathTracker.BuildTargetPath,
                targetPlatform,
                finalBuildOptions
            );

            buildReport._totalTimeTicks = (ulong)(DateTime.Now - startTime).Ticks;
            if (!string.IsNullOrEmpty(errorMessage))
            {
                buildReport.Result = BuildResult.Failed;
                buildReport.ErrorMessage = errorMessage;
                Debug.LogError(errorMessage);
            }
            else
            {
                buildReport.Result = BuildResult.Succeeded;
            }

            return buildReport;
#endif
        }

        private BuildTarget _GetBuildTargetPlatform()
        {
            return _isAnyPlatformBuilder ? EditorUserBuildSettings.activeBuildTarget : _buildTargetPlatform;
        }

        private static string[] _GetAllActivatedSceneNames()
        {
            // Not using EditorBuildSettingsScene.GetActiveSceneList() to support Unity 4
            // It is the same Linq under the hood
            return EditorBuildSettings.scenes.Where(scene => scene.enabled).Select(scene => scene.path).ToArray();
        }


        /// <summary>
        /// Dump all descriptive metadata, including metadata of build steps
        /// </summary>
        /// <returns>Metadata</returns>
        public BuilderMetadata GetMetadata()
        {
            return new BuilderMetadata
                   {
                       Description = _description,
                       BuildSteps = _childrenSteps.Select(childStep => childStep.GetMetadata()).ToList()
                   };
        }

        private void _PrintHelp()
        {
            BuilderMetadata metadata = GetMetadata();

            var stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Builder: {0}\n", metadata.Description);
            stringBuilder.AppendLine("Steps:");
            foreach (BuildStepMetadata stepMetadata in metadata.BuildSteps)
            {
                stringBuilder.AppendFormat("\t{0}:\n", stepMetadata.Description);
                foreach (ParameterMetadata paramMetadata in stepMetadata.ParameterList)
                {
                    stringBuilder.AppendFormat("\t\t{0} <{1}>\n", paramMetadata.Flag, paramMetadata.Type);
                }
            }

            Debug.Log(stringBuilder.ToString());
        }

        private static void _PrintCommandlineArguments(CommandLineArguments arguments)
        {
            Debug.LogFormat("Commandline Arguments:\n{0}", arguments.ToString());
        }
    }
}