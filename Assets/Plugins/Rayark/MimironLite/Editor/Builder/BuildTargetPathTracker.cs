namespace Rayark.MimironLite.Build
{
    /// <summary>
    /// Helper class for tracking set Unity build target path 
    /// </summary>
    public class BuildTargetPathTracker
    {
        /// <value>
        /// Build target path  
        /// BuildPipeline.BuildPlayer can handle both relative and absolute path  
        /// Relative path base is Unity project root folder  
        /// </value>
        public string BuildTargetPath;
    }
}