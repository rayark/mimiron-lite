#if UNITY_2018_1_OR_NEWER
#define BUILD_REPORT_API
#endif

namespace Rayark.MimironLite.Build
{
    using System;
    using UnityEditor;

#if !BUILD_REPORT_API
    public enum BuildType
    {
        Player = 1,
        AssetBundle = 2
    }

    /// <summary>
    ///     <para>Describes the outcome of the build process.</para>
    /// </summary>
    public enum BuildResult
    {
        /// <summary>
        ///     <para>Indicates that the outcome of the build is in an unknown state.</para>
        /// </summary>
        Unknown,

        /// <summary>
        ///     <para>Indicates that the build completed successfully.</para>
        /// </summary>
        Succeeded,

        /// <summary>
        ///     <para>Indicates that the build failed.</para>
        /// </summary>
        Failed,

        /// <summary>
        ///     <para>Indicates that the build was cancelled by the user.</para>
        /// </summary>
        Cancelled
    }

    /// <summary>
    ///     <para>The BuildReport API gives you information about the Unity build process.</para>
    /// </summary>
    public sealed class BuildReport
    {
        internal long _buildStartTimeTicks;
        internal ulong _totalTimeTicks;

        /// <summary>
        ///     <para>The time the build was started.</para>
        /// </summary>
        public DateTime BuildStartedAt
        {
            get { return new DateTime(_buildStartTimeTicks); }
        }

        /// <summary>
        ///     <para>The platform that the build was created for.</para>
        /// </summary>
        public BuildTarget Platform { get; set; }

        /// <summary>
        ///     <para>The platform group the build was created for.</para>
        /// </summary>
        public BuildTargetGroup PlatformGroup { get; set; }

        /// <summary>
        ///     <para>The BuildOptions used for the build, as passed to BuildPipeline.BuildPlayer.</para>
        /// </summary>
        public BuildOptions Options { get; set; }

        /// <summary>
        ///     <para>The output path for the build, as provided to BuildPipeline.BuildPlayer.</para>
        /// </summary>
        public string OutputPath { get; set; }

        /// <summary>
        ///     <para>The total time taken by the build process.</para>
        /// </summary>
        public TimeSpan TotalTime
        {
            get { return new TimeSpan((long)_totalTimeTicks); }
        }

        /// <summary>
        ///     <para>The time the build ended.</para>
        /// </summary>
        public DateTime BuildEndedAt
        {
            get { return BuildStartedAt + TotalTime; }
        }

        /// <summary>
        ///     <para>An error message if an error occurred during the build process.</para>
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        ///     <para>The outcome of the build.</para>
        /// </summary>
        public BuildResult Result { get; set; }

        public BuildType BuildType { get; set; }
    }

#endif
}