using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting bundle version, it will affect both Android and iOS  
    /// This build step is kept for backward compatibility, not recommended for new build pipeline  
    /// Use <see cref="Android.SetAndroidVersionNameBuildStep"/> or <see cref="iOS.SetiOSVersionStringBuildStep"/> to avoid confusion  
    /// -bundleVersion [string]  
    /// </summary>
    public class SetBundleVersionBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-bundleVersion")]
            public string BundleVersion;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            playerSettings.BundleVersion = parameter.BundleVersion;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.Any;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set bundle version (CFShortBundleVersionString on iOS, VersionName on Android)";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}