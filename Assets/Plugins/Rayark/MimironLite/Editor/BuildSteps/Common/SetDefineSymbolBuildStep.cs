using System.Collections.Generic;


namespace Rayark.MimironLite.BuildStep
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// Build step of adding/removing C# define symbol  
    /// -[User defined flag] [bool]  
    /// "true" to add and "false" to remove  
    /// </summary>
    public class SetDefineSymbolBuildStep : IBuildStep
    {
        private readonly string _flag;
        private readonly string _defineSymbol;

        /// <summary>
        /// Constructor of SetDefineSymbolBuildStep 
        /// </summary>
        /// <param name="flag">Flag that trigger define symbol add/remove</param>
        /// <param name="defineSymbol">Define symbol to be added/removed</param>
        public SetDefineSymbolBuildStep
        (
            string flag,
            string defineSymbol
        )
        {
            _flag = flag;
            _defineSymbol = defineSymbol;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            bool hasDefineSymbol = commandLineArguments.RetrieveArgumentAsBool(_flag);

            string symbols = playerSettings.GetDefineSymbols();
            var symbolCollection = DefineSymbolCollection.FromUnityDefineSymbolsString(symbols);

            if (hasDefineSymbol)
            {
                symbolCollection.Add(_defineSymbol);
            }
            else
            {
                symbolCollection.Remove(_defineSymbol);
            }

            playerSettings.SetDefineSymbols(DefineSymbolCollection.ToUnityDefineSymbolsString(symbolCollection));
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.Any;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            var recordedParameter = new ParameterMetadata
                                    {
                                        Flag = _flag,
                                        Type = ParameterType.Bool
                                    };

            string description = string.Format("Set Define Symbol BuildStep ({0})", _defineSymbol);
            var parameterList = new List<ParameterMetadata> {recordedParameter};

            return new BuildStepMetadata(description, parameterList);
        }
    }
}