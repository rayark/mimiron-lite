#if UNITY_5_1_1 || UNITY_5_1_2 || UNITY_5_1_3 || UNITY_5_1_4 || UNITY_5_1_5 || UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_1_1_OR_NEWER
#endif

#if UNITY_5_1_1_OR_NEWER
#define GPU_SKINNING_SETTER_AVAILABLE
#endif


#if GPU_SKINNING_SETTER_AVAILABLE
using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting is using GPU skinning or not  
    /// -GPUSkinning [bool]  
    /// </summary>
    public class SetGpuSkinningBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-GPUSkinning")]
            public bool IsUsingGpuSkinning;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            playerSettings.IsGpuSkinning = parameter.IsUsingGpuSkinning;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.Any;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set is using GPU skinning";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}
#endif