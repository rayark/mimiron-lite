using System.IO;

using JetBrains.Annotations;

namespace Rayark.MimironLite.BuildStep
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting build target path  
    /// -targetPath [string]  
    /// </summary>
    public class SetBuildTargetPathBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-targetPath")]
            public string Path;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            // Remove trailing slash
            // You should not pass -targetPath with trailing slash for iOS build even though it creates a Xcode project folder
            // If there is a trailing slash in build target path, Unity would behave normally,
            // but it will pass the slash to Google CocoaPods post-processor and breaks xcworkspace patching
            buildTargetPathTracker.BuildTargetPath = parameter.Path.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.Any;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set path of build target";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}