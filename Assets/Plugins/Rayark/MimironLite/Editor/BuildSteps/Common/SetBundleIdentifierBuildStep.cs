namespace Rayark.MimironLite.BuildStep
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting bundle identifier  
    /// -bundleIdentifier [string]  
    /// </summary>
    public class SetBundleIdentifierBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [CommandlineFlag("-bundleIdentifier")]
            public string BundleIdentifier;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            playerSettings.BundleIdentifier = parameter.BundleIdentifier;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.Any;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set bundle identifier";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}