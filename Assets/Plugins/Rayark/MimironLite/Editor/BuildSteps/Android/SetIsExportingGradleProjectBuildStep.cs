#if UNITY_5 || UNITY_5_3_OR_NEWER
#define UNITY_5_OR_NEWER
#endif

#if UNITY_5_OR_NEWER
#define EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE
#endif

#if UNITY_5_5_OR_NEWER
#define ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
#endif


#if EXPORT_AS_GOOGLE_ANDROID_PROJECT_AVAILABLE && ANDROID_BUILD_SYSTEM_OPTION_AVAILABLE
using UnityEditor;

using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep.Android
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting is exporting as Gradle project or not  
    /// -exportGradle [bool]  
    /// </summary>
    public class SetIsExportingGradleProjectBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-exportGradle")]
            public bool IsExportingGradleProject;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            if (parameter.IsExportingGradleProject)
            {
                playerSettings.AndroidBuildSystem = AndroidBuildSystem.Gradle;
                playerSettings.IsExportingAsGoogleAndroidProject = true;
                buildOptionsTracker.Add(BuildOptions.AcceptExternalModificationsToPlayer);
            }
            else
            {
                playerSettings.IsExportingAsGoogleAndroidProject = false;
                buildOptionsTracker.Remove(BuildOptions.AcceptExternalModificationsToPlayer);
            }
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.AndroidOnly;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set is exporting gradle";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}
#endif