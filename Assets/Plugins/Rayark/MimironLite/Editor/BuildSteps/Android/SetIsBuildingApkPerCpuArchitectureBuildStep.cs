#if UNITY_2018_2_OR_NEWER
#define BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
#endif


#if BUILD_APK_PER_CPU_ARCHITECTURE_AVAILABLE
using JetBrains.Annotations;

namespace Rayark.MimironLite.BuildStep.Android
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    public class SetIsBuildingApkPerCpuArchitectureBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-multipleAPKsPerABI")]
            public bool IsBuildingApkPerCpu;
        }

        public void Execute
        (
            CommandLineArguments commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker buildTargetPathTracker,
            BuildOptionsTracker buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            playerSettings.IsBuildingApkPerCpuArchitecture = parameter.IsBuildingApkPerCpu;
        }

        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.AndroidOnly;
        }

        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set is building multiple APK by ABI";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}
#endif