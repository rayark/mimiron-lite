using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep.Android
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting Android version name  
    /// Android version name is a string visible to all user  
    /// -androidVersionName [string]  
    /// </summary>
    public class SetAndroidVersionNameBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-androidVersionName")]
            public string VersionName;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            // Has side effect of setting iOS CFShortBundleVersionString
            playerSettings.BundleVersion = parameter.VersionName;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.AndroidOnly;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set Android version name (string, display to users)";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}