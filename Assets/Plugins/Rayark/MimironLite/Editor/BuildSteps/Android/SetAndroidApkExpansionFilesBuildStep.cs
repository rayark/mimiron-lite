using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep.Android
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting is using OBB file or not  
    /// -generateOBB [bool]  
    /// </summary>
    public class SetAndroidApkExpansionFilesBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-generateOBB")]
            public bool IsUsingApkExpansionFiles;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            playerSettings.IsUsingApkExpansionFiles = parameter.IsUsingApkExpansionFiles;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.AndroidOnly;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set is generating OBB file";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}