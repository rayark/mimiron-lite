using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep.Android
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting Android keystore unlock username and password  
    /// -keystoreName [string] (Path to Android keystore file)  
    /// -keystorePass [string]  
    /// -keyaliasName [string]  
    /// -keyaliasPass [string]  
    /// </summary>
    public class SetAndroidKeyStoreBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-keystoreName")]
            public string KeystoreName;

            [UsedImplicitly]
            [CommandlineFlag("-keystorePass")]
            public string KeystorePass;

            [UsedImplicitly]
            [CommandlineFlag("-keyaliasName")]
            public string KeyaliasName;

            [UsedImplicitly]
            [CommandlineFlag("-keyaliasPass")]
            public string KeyaliasPass;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            playerSettings.UseCustomKeystore = true;
            playerSettings.AndroidKeystoreName = parameter.KeystoreName;
            playerSettings.AndroidKeystorePass = parameter.KeystorePass;
            playerSettings.AndroidKeyaliasName = parameter.KeyaliasName;
            playerSettings.AndroidKeyaliasPass = parameter.KeyaliasPass;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.AndroidOnly;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set keystore path, keystore password, keyalias name and keyalias password";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}