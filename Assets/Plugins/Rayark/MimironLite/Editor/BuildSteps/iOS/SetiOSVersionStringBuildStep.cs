using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep.iOS
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    // Setting property CFBundleShortVersionString
    // Not naming it BundleVersion since build number property is CFBundleVersion

    /// <summary>
    /// build step of setting iOS version string  
    /// It's the string in Xcode identity/Version  
    /// It will end up assign to <see href="https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleshortversionstring">CFBundleShortVersionString</see> in plist  
    /// -iOSVersionString [string]  
    /// </summary>
    public class SetiOSVersionStringBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-iOSVersionString")]
            public string iOSVersion;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            // Has side effect of setting Android Version Name
            playerSettings.BundleVersion = parameter.iOSVersion;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.iOSOnly;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description =
                "Set iOS version string (x.y.z format, has to match iTunes Connect, \"Version\" in Xcode, CFBundleShortVersionString)";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}