#if UNITY_5_2 || UNITY_5_3_OR_NEWER
#define UNITY_5_2_OR_NEWER
#endif

#if UNITY_5_2_OR_NEWER
#define IOS_BUILD_NUMBER_AVAILABLE
#endif

#if IOS_BUILD_NUMBER_AVAILABLE
using JetBrains.Annotations;


namespace Rayark.MimironLite.BuildStep.iOS
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// build step of setting iOS build string  
    /// It's the string in Xcode identity/Build  
    /// It will end up assign to <see href="https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleversion">CFBundleVersion</see> in plist  
    /// -iOSBuildString [string]  
    /// </summary>
    public class SetiOSBuildStringBuildStep : IBuildStep
    {
        private class Parameter : IBuildStepParameter
        {
            [UsedImplicitly]
            [CommandlineFlag("-iOSBuildString")]
            public string iOSBuild;
        }

        /// <inheritdoc/>
        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            var parameter = new Parameter();
            ParameterUtilities.ParseParameters(commandLineArguments, parameter);

            playerSettings.iOSBuildNumber = parameter.iOSBuild;
        }

        /// <inheritdoc/>
        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.iOSOnly;
        }

        /// <inheritdoc/>
        public BuildStepMetadata GetMetadata()
        {
            const string description = "Set iOS build string (x.y.z format, \"Build\" in Xcode, CFBundleVersion)";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }
}
#endif