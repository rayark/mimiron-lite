namespace Rayark.MimironLite
{
    /// <summary>
    /// Interface for marking a data structure to be auto-filled by <see cref="ParameterUtilities"/>  
    /// </summary>
    public interface IBuildStepParameter { }
}