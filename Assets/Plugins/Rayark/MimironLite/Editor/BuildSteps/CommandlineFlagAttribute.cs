using System;


namespace Rayark.MimironLite
{
    /// <summary>
    /// Attribute tag for automatic command line parsing in <see cref="ParameterUtilities"/>
    /// </summary>
    public class CommandlineFlagAttribute : Attribute
    {
        /// <value>Flag to retrieve command line argument and fill into the field</value>
        public string Flag        { get; private set; }
        /// <value>Description for building metadata</value>
        public string Description { get; private set; }

        /// <param name="flag">Flag for retrieving argument</param>
        /// <param name="description">Description for metadata</param>
        public CommandlineFlagAttribute(string flag, string description = "")
        {
            Flag = flag;
            Description = description;
        }
    }
}