namespace Rayark.MimironLite.BuildStep
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    /// <summary>
    /// Base interface of all build steps
    /// </summary>
    public interface IBuildStep
    {
        /// <summary>
        /// Execute the build step, should be invoked by Builder instances
        /// </summary>
        /// <param name="commandLineArguments">Wrapped commandline arguments</param>
        /// <param name="playerSettings">PlayerSettingsManipulator instance</param>
        /// <param name="buildTargetPathTracker">Tracker instance for build target path</param>
        /// <param name="buildOptionsTracker">Tracker instance for Unity build option flags</param>
        void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        );

        /// <summary>
        /// List applicable Unity build targets of build step
        /// </summary>
        /// <returns>A collection of applicable BuildTarget</returns>
        BuildTargetCollection GetApplicableBuildTargets();

        /// <summary>
        /// Get descriptive metadata of build step and all metadata of its commandline parameters
        /// </summary>
        /// <returns>Description in BuildStepMetadata object</returns>
        BuildStepMetadata GetMetadata();
    }
}