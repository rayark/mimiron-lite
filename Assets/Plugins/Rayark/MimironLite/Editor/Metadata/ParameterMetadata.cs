using System;
using System.Linq;
using System.Reflection;


namespace Rayark.MimironLite.Metadata
{
    /// <summary>
    /// Supported build step parameter types
    /// </summary>
    public enum ParameterType
    {
        /// <summary>Cannot determine parameter type</summary>
        Error,
        String,
        Bool,
        Int,
        Float
    }

    /// <summary>
    /// Metadata of a build step parameter  
    /// </summary>
    [Serializable]
    public class ParameterMetadata
    {
        ///<value>Type of parameter</value>
        public ParameterType Type;
        ///<value>Command line flag of parameter</value>
        public string        Flag;
        ///<value>Description of parameter</value>
        public string        Description;

        public ParameterMetadata() {}

        /// <summary>
        /// Constructor for building metadata from reflection
        /// </summary>
        /// <param name="fieldInfo">FieldInfo of field tagged with CommandlineFlagAttribute</param>
        public ParameterMetadata(FieldInfo fieldInfo)
        {
            Type = _GetParameterType(fieldInfo.FieldType);

            var commandlineFlagAttribute = fieldInfo.GetCustomAttributes(inherit: false)
                                                   .FirstOrDefault(attr => attr is CommandlineFlagAttribute)
                                                                                as CommandlineFlagAttribute;

            if (commandlineFlagAttribute != null)
            {
                Flag        = commandlineFlagAttribute.Flag;
                Description = commandlineFlagAttribute.Description;
            }
        }

        private static ParameterType _GetParameterType(Type type)
        {
            if (type == typeof(string))
            {
                return ParameterType.String;
            }

            if (type == typeof(int))
            {
                return ParameterType.Int;
            }

            if (type == typeof(float))
            {
                return ParameterType.Float;
            }

            if (type == typeof(bool))
            {
                return ParameterType.Bool;
            }

            return ParameterType.Error;
        }
    }
}