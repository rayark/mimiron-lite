using System;
using System.Collections.Generic;


namespace Rayark.MimironLite.Metadata
{
    /// <summary>
    /// Builder metadata describing a builder and metadata of all containing build steps
    /// </summary>
    [Serializable]
    public class BuilderMetadata
    {
        /// <value>Builder description</value>
        public string                  Description;
        /// <value>Metadata of child build steps</value>
        public List<BuildStepMetadata> BuildSteps = new List<BuildStepMetadata>();
    }
}