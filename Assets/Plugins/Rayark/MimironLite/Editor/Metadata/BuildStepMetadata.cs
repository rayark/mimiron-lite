using System;
using System.Collections.Generic;


namespace Rayark.MimironLite.Metadata
{
    /// <summary>
    /// Build step metadata describing a build step and all parameters of this build step
    /// </summary>
    [Serializable]
    public class BuildStepMetadata
    {
        /// <value>Build step description</value>
        public string                  Description;
        /// <value>Metadata of build step parameters</value>
        public List<ParameterMetadata> ParameterList;

        /// <summary>
        /// BuildStepMetadata constructor
        /// </summary>
        /// <param name="description">Build step description</param>
        /// <param name="parameterList">Parameter metadata list</param>
        public BuildStepMetadata(string description, List<ParameterMetadata> parameterList)
        {
            Description   = description;
            ParameterList = parameterList;
        }
    }
}