using NUnit.Framework;


namespace Rayark.MimironLite.Tests
{
    [TestFixture]
    public class CommandlineArgumentsTests
    {
        [Test]
        public void HasArgument_ArgumentNotExist_ReturnFalse()
        {
            string[] testCase = {"-flagA", "ArgA", "-flagB", "ArgB", "-flagC", "ArgC"};

            var commandlineArguments = new CommandLineArguments(testCase);
            bool hasArgument = commandlineArguments.HasArgument("-flagZ");

            Assert.False(hasArgument);
        }

        [Test]
        public void HasArgument_ArgumentExist_ReturnTrue()
        {
            string[] testCase = {"-flagA", "ArgA", "-flagB", "ArgB", "-flagC", "ArgC"};

            var commandlineArguments = new CommandLineArguments(testCase);
            bool hasArgument = commandlineArguments.HasArgument("-flagB");

            Assert.True(hasArgument);
        }


        [Test]
        public void RetrieveArgument_FlagNotExist_ThrowMissingArgument()
        {
            string[] testCase = {"-stringFlag", "someStringValue"};

            var commandlineArguments = new CommandLineArguments(testCase);

            var ex = Assert.Throws<MissingArgumentException>(
                () => commandlineArguments.RetrieveArgument("-stringFlag2"));
            Assert.AreEqual("-stringFlag2", ex.Flag);
        }

        [Test]
        public void RetrieveArgument_ArgumentOmitted_ThrowMissingArgument()
        {
            string[] testCase = {"-stringFlag"};

            var commandlineArguments = new CommandLineArguments(testCase);

            var ex = Assert.Throws<MissingArgumentException>(
                () => commandlineArguments.RetrieveArgument("-stringFlag"));
            Assert.AreEqual("-stringFlag", ex.Flag);
        }

        [Test]
        public void RetrieveArgument_ArgumentNotExistFollowedByNextFlag_ThrowMissingArgument()
        {
            string[] testCase = {"-stringFlag", "-someOtherFlag"};

            var commandlineArguments = new CommandLineArguments(testCase);

            var ex = Assert.Throws<MissingArgumentException>(
                () => commandlineArguments.RetrieveArgument("-stringFlag"));
            Assert.AreEqual("-stringFlag", ex.Flag);
        }

        [Test]
        public void RetrieveArgument_OmitFlagDash_ReturnValue()
        {
            string[] testCase = {"-stringFlag", "someStringValue"};

            var commandlineArguments = new CommandLineArguments(testCase);
            string retrievedString = commandlineArguments.RetrieveArgument("stringFlag");

            Assert.AreEqual(retrievedString, "someStringValue");
        }

        [Test]
        public void RetrieveArgument_ArgumentExist_ReturnValue()
        {
            string[] testCase = {"-stringFlag", "someStringValue"};

            var commandlineArguments = new CommandLineArguments(testCase);
            string retrievedString = commandlineArguments.RetrieveArgument("-stringFlag");

            Assert.AreEqual(retrievedString, "someStringValue");
        }


        [Test]
        public void RetrieveArgumentAsBool_ArgumentNotExist_ThrowMissingArgument()
        {
            string[] testCase = {"-yesOrNo", "True"};

            var commandlineArgument = new CommandLineArguments(testCase);

            var ex = Assert.Throws<MissingArgumentException>(
                () => commandlineArgument.RetrieveArgumentAsBool("-noOrNo"));
            Assert.AreEqual(ex.Flag, "-noOrNo");
        }

        [Test]
        public void RetrieveArgumentAsBool_RandomString_ThrowInvalidFormat()
        {
            string[] testCase = {"-yesOrNo", "Zh?"};

            var commandlineArgument = new CommandLineArguments(testCase);

            var ex = Assert.Throws<InvalidFormatException>(
                () => commandlineArgument.RetrieveArgumentAsBool("-yesOrNo"));
            Assert.AreEqual(ex.Flag, "-yesOrNo");
            Assert.AreEqual(ex.GivenString, "Zh?");
        }

        [Test]
        public void RetrieveArgumentAsBool_TrueLowerCase_ReturnTrue()
        {
            string[] testCase = {"-yesOrNo", "true"};

            var commandlineArguments = new CommandLineArguments(testCase);
            bool retrievedBool = commandlineArguments.RetrieveArgumentAsBool("-yesOrNo");

            Assert.True(retrievedBool);
        }

        [Test]
        public void RetrieveArgumentAsBool_TrueUpperCase_ReturnTrue()
        {
            string[] testCase = {"-yesOrNo", "TRUE"};

            var commandlineArguments = new CommandLineArguments(testCase);
            bool retrievedBool = commandlineArguments.RetrieveArgumentAsBool("-yesOrNo");

            Assert.True(retrievedBool);
        }

        [Test]
        public void RetrieveArgumentAsBool_FalseMixedCase_ReturnFalse()
        {
            string[] testCase = {"-yesOrNo", "fAlSe"};

            var commandlineArgument = new CommandLineArguments(testCase);
            bool retrievedBool = commandlineArgument.RetrieveArgumentAsBool("-yesOrNo");

            Assert.False(retrievedBool);
        }


        [Test]
        public void RetrieveArgumentAsInt_ArgumentNotExist_ThrowMissingArgument()
        {
            string[] testCase = {"-someIntFlag"};

            var commandlineArgument = new CommandLineArguments(testCase);

            var ex = Assert.Throws<MissingArgumentException>(
                () => commandlineArgument.RetrieveArgumentAsBool("-someIntFlag"));
            Assert.AreEqual(ex.Flag, "-someIntFlag");
        }

        [Test]
        public void RetrieveArgumentAsInt_ArgumentNotIntString_ReturnNull()
        {
            string[] testCase = {"-someIntFlag", "IAmNotInt"};

            var commandlineArgument = new CommandLineArguments(testCase);

            var ex = Assert.Throws<InvalidFormatException>(
                () => commandlineArgument.RetrieveArgumentAsBool("-someIntFlag"));
            Assert.AreEqual(ex.Flag, "-someIntFlag");
            Assert.AreEqual(ex.GivenString, "IAmNotInt");
        }

        [Test]
        public void RetrieveArgumentAsInt_IntString_ReturnValue()
        {
            string[] testCase = {"-someIntFlag", "42"};

            var commandlineArgument = new CommandLineArguments(testCase);
            int retrievedInt = commandlineArgument.RetrieveArgumentAsInt("-someIntFlag");

            Assert.AreEqual(42, retrievedInt);
        }


        [Test]
        public void RetrieveArgumentAsFloat_ArgumentNotExist_ReturnNull()
        {
            string[] testCase = {"-someFloatFlag"};

            var commandlineArgument = new CommandLineArguments(testCase);

            var ex = Assert.Throws<MissingArgumentException>(
                () => commandlineArgument.RetrieveArgumentAsBool("-someFloatFlag"));
            Assert.AreEqual(ex.Flag, "-someFloatFlag");
        }

        [Test]
        public void RetrieveArgumentAsFloat_ArgumentNotFloatString_ReturnNull()
        {
            string[] testCase = {"-someFloatFlag", "IAmNotFloat"};

            var commandlineArgument = new CommandLineArguments(testCase);

            var ex = Assert.Throws<InvalidFormatException>(
                () => commandlineArgument.RetrieveArgumentAsBool("-someFloatFlag"));
            Assert.AreEqual(ex.Flag, "-someFloatFlag");
            Assert.AreEqual(ex.GivenString, "IAmNotFloat");
        }

        [Test]
        public void RetrieveArgumentAsFloat_FloatString_ReturnValue()
        {
            string[] testCase = {"-someFloatFlag", "3.14159"};

            var commandlineArgument = new CommandLineArguments(testCase);
            float retrievedFloat = commandlineArgument.RetrieveArgumentAsFloat("-someFloatFlag");

            Assert.AreEqual(3.14159f, retrievedFloat, float.Epsilon);
        }
    }
}