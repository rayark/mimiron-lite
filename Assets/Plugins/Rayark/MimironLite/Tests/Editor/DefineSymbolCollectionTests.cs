using NUnit.Framework;


namespace Rayark.MimironLite.Tests
{
    [TestFixture]
    public class DefineSymbolCollectionTests
    {
        [Test]
        public void ConvertFromUnitySymbols_BackToUnitySymbols_Same()
        {
            const string testCase = "AAA;BSymbol;SomeCSymbol";

            var collection = DefineSymbolCollection.FromUnityDefineSymbolsString(testCase);
            string composeResult = DefineSymbolCollection.ToUnityDefineSymbolsString(collection);

            Assert.AreEqual(testCase, composeResult);
        }

        [Test]
        public void ConvertFromUnitySymbols_TrialingSemicolon_Remove()
        {
            const string testCase = "AAA;BSymbol;SomeCSymbol;;";

            var collection = DefineSymbolCollection.FromUnityDefineSymbolsString(testCase);
            string composeResult = DefineSymbolCollection.ToUnityDefineSymbolsString(collection);

            Assert.AreEqual("AAA;BSymbol;SomeCSymbol", composeResult);
        }

        [Test]
        public void ConvertFromUnitySymbols_PrependingSemicolon_Remove()
        {
            const string testCase = ";;AAA;BSymbol;SomeCSymbol";

            var collection = DefineSymbolCollection.FromUnityDefineSymbolsString(testCase);
            string composeResult = DefineSymbolCollection.ToUnityDefineSymbolsString(collection);

            Assert.AreEqual("AAA;BSymbol;SomeCSymbol", composeResult);
        }

        [Test]
        public void ConvertFromUnitySymbols_DuplicatedSymbol_Remove()
        {
            const string testCase = "AAA;BSymbol;SomeCSymbol;SomeCSymbol;BSymbol;AAA";

            var collection = DefineSymbolCollection.FromUnityDefineSymbolsString(testCase);
            string composeResult = DefineSymbolCollection.ToUnityDefineSymbolsString(collection);

            Assert.AreEqual("AAA;BSymbol;SomeCSymbol", composeResult);
        }

        [Test]
        public void AddSymbol_MultipleAdd_AppendAtTheEndOrdered()
        {
            const string testCase = "AAA;BSymbol;SomeCSymbol";

            var collection = DefineSymbolCollection.FromUnityDefineSymbolsString(testCase);
            collection.Add("DSymbol");
            collection.Add("EEE");
            string composeResult = DefineSymbolCollection.ToUnityDefineSymbolsString(collection);

            Assert.AreEqual("AAA;BSymbol;SomeCSymbol;DSymbol;EEE", composeResult);
        }

        [Test]
        public void RemoveSymbol_Remove_DoDisappear()
        {
            const string testCase = "AAA;BSymbol;SomeCSymbol";

            var collection = DefineSymbolCollection.FromUnityDefineSymbolsString(testCase);
            collection.Remove("BSymbol");
            string composeResult = DefineSymbolCollection.ToUnityDefineSymbolsString(collection);

            Assert.AreEqual("AAA;SomeCSymbol", composeResult);
        }

        [Test]
        public void ManipulateSymbols_AddThenRemove_NoChange()
        {
            const string testCase = "AAA;BSymbol;SomeCSymbol";

            var collection = DefineSymbolCollection.FromUnityDefineSymbolsString(testCase);
            collection.Add("DSymbol");
            collection.Remove("DSymbol");
            string composeResult = DefineSymbolCollection.ToUnityDefineSymbolsString(collection);

            Assert.AreEqual(testCase, composeResult);
        }
    }
}