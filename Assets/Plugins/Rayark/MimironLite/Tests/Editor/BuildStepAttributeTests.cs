using System;
using System.Linq;
using System.Reflection;

using NUnit.Framework;


namespace Rayark.MimironLite.Tests
{
    using Rayark.MimironLite.Build;
    using Rayark.MimironLite.BuildStep;
    using Rayark.MimironLite.Metadata;
    using Rayark.MimironLite.PlayerSettings;

    public class AttributeTestBuildStep : IBuildStep
    {
        internal class Parameter : IBuildStepParameter
        {
            [CommandlineFlag("someflag", "good?")]
            public string SomeStringFromCommandline;
        }

        public void Execute
        (
            CommandLineArguments       commandLineArguments,
            IPlayerSettingsManipulator playerSettings,
            BuildTargetPathTracker     buildTargetPathTracker,
            BuildOptionsTracker        buildOptionsTracker
        )
        {
            throw new NotImplementedException();
        }

        public BuildTargetCollection GetApplicableBuildTargets()
        {
            return BuildTargetCollection.Any;
        }

        public BuildStepMetadata GetMetadata()
        {
            const string description = "I am a test stub";
            var parameterList = ParameterUtilities.GetMetadataList(typeof(Parameter));

            return new BuildStepMetadata(description, parameterList);
        }
    }

    [TestFixture]
    public class BuildStepAttributeTests
    {
        [Test]
        public void TestCustomAttributeBehaviour()
        {
            Type[] buildStepParameterTypes =
                typeof(AttributeTestBuildStep).GetNestedTypes(BindingFlags.NonPublic | BindingFlags.Public)
                                              .Where(nestedType =>
                                                         typeof(IBuildStepParameter).IsAssignableFrom(nestedType))
                                              .ToArray();

            Assert.AreEqual(1, buildStepParameterTypes.Length);

            FieldInfo testFieldInfo = buildStepParameterTypes[0]
                                      .GetFields(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault();
            var parsingEntryParameterAttribute =
                testFieldInfo.GetCustomAttributes(inherit: false)
                             .FirstOrDefault(attr => attr is CommandlineFlagAttribute) 
                                                          as CommandlineFlagAttribute;

            Assert.AreEqual(parsingEntryParameterAttribute.Flag, "someflag");
            Assert.AreEqual(parsingEntryParameterAttribute.Description, "good?");

            var parameter = new AttributeTestBuildStep.Parameter();
            testFieldInfo.SetValue(parameter, "testStringValue");
            Assert.AreEqual("testStringValue", parameter.SomeStringFromCommandline);
        }
    }
}